﻿using System.CodeDom.Compiler;
using System.Collections.Immutable;
using System.Diagnostics;
using System.Globalization;
using System.Reflection;
using System.Text;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Text;

namespace JRMC.Docker.WebApi.Generators;

/// <summary>
/// Generates an interface IRegisterEndpoint and Extension method RegisterEndpoints to register all implementations.
/// </summary>
[Generator]
internal sealed class RegisterEndpointsGenerator : IIncrementalGenerator
{
    private static readonly AssemblyName AssemblyName = typeof(RegisterEndpointsGenerator).Assembly.GetName();

    private static readonly string GeneratedCodeAttribute =
        $"""global::System.CodeDom.Compiler.GeneratedCodeAttribute("{AssemblyName.Name}", "{AssemblyName.Version}")""";
    
    private static readonly string RegisterEndpoint =
        $$"""
        // <auto-generated/>
        #nullable enable
        
        [{{GeneratedCodeAttribute}}]
        public interface IRegisterEndpoint
        {
            static abstract void Register(WebApplication app);
        }
        """;
    
    public void Initialize(IncrementalGeneratorInitializationContext context)
    {
        context.RegisterPostInitializationOutput(static ctx =>
        {
            ctx.AddSource("IRegisterEndpoint.g.cs", SourceText.From(RegisterEndpoint, Encoding.UTF8));
        });
        
        var compilation = context.SyntaxProvider.CreateSyntaxProvider(
                predicate: static (node, _) => node is ClassDeclarationSyntax { BaseList: not null } x
                                               // There is probably a better way to do this, this will only look at the symbol name
                                               && x.BaseList.Types.Any(x => x.Type.ToString() == "IRegisterEndpoint"),
                transform: static (ctx, _) => (ClassDeclarationSyntax)ctx.Node)
            .Collect();
        
        context.RegisterSourceOutput(compilation, Execute);
    }

    private static void Execute(SourceProductionContext context, ImmutableArray<ClassDeclarationSyntax> classes)
    {
        var builder = new StringBuilder();
        using var writer = new StringWriter(builder, CultureInfo.InvariantCulture);
        using var source = new IndentedTextWriter(writer, "\t");
        
        source.WriteLine("// <auto-generated/>");
        source.WriteLine("#nullable enable");
        source.WriteLine();
        source.WriteLine($"namespace {AssemblyName.Name};");
        source.WriteLine();
        
        source.WriteLine($"[{GeneratedCodeAttribute}]");
        source.WriteLine("public static class RegisterEndpointsExtensions");
        source.WriteLine("{");
        source.Indent++;
        
        source.WriteLine("public static void RegisterEndpoints(this global::Microsoft.AspNetCore.Builder.WebApplication app)");
        source.WriteLine("{");
        source.Indent++;

        foreach (var c in classes)
        {
            source.WriteLine($"global::{GetNamespace(c)}.{c.Identifier.Text}.Register(app);");
        }
        
        source.Indent--;
        source.WriteLine("}");

        source.Indent--;
        source.WriteLine("}");
        
        Debug.Assert(source.Indent == 0);
        
        context.AddSource("RegisterEndpointsExtensions.g.cs", writer.ToString());
    }
    
    private static string GetNamespace(ClassDeclarationSyntax syntax)
    {
        var nameSpace = new StringBuilder();
        
        var potentialNamespaceParent = syntax.Parent;
        
        while (potentialNamespaceParent != null &&
               potentialNamespaceParent is not NamespaceDeclarationSyntax
               && potentialNamespaceParent is not FileScopedNamespaceDeclarationSyntax)
        {
            potentialNamespaceParent = potentialNamespaceParent.Parent;
        }
        
        if (potentialNamespaceParent is BaseNamespaceDeclarationSyntax namespaceParent)
        {
            nameSpace.Append(namespaceParent.Name);
            
            while (true)
            {
                if (namespaceParent.Parent is not NamespaceDeclarationSyntax parent)
                {
                    break;
                }
                
                nameSpace.Insert(0, '.');
                nameSpace.Insert(0, namespaceParent.Name);
                namespaceParent = parent;
            }
        }
        
        return nameSpace.ToString();
    }

}
