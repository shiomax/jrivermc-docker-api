﻿using JRMC.Docker.DataAccess.Contracts;
using JRMC.Docker.DataAccess.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace JRMC.Docker.DataAccess;

public static class ServiceProviderExtensions
{
    public static void AddSqliteServices(this IServiceCollection services, IDatabaseSettings settings)
    {
        services.AddSingleton<IUserRepository, UserRepository>();
        services.AddSingleton<IRefreshTokenRepository, RefreshTokenRepository>();
    }
}