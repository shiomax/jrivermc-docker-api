DROP TABLE IF EXISTS RefreshToken;
DROP TABLE IF EXISTS KeyValueStore;

CREATE TABLE IF NOT EXISTS "AppUser" (
     "Id" INTEGER PRIMARY KEY,
     "Username" TEXT UNIQUE NOT NULL,
     "Password" TEXT NOT NULL,
     "UserRole" INTEGER NOT NULL, 
     "EnableTwoFa" INTEGER NOT NULL DEFAULT 0);

CREATE TABLE "RefreshToken" (
    "Id" INTEGER NOT NULL PRIMARY KEY,
    "Token" TEXT NOT NULL,
    "JwtId" TEXT NOT NULL,
    "CreationTime" TEXT NOT NULL,
    "ExpiryTime" TEXT NOT NULL,
    "UserId" INTEGER NOT NULL
);

CREATE TABLE IF NOT EXISTS "TwoFactor" (
     "Id" INTEGER NOT NULL PRIMARY KEY,
     "Secret" BLOB NOT NULL,
     "TwoFactorMethod" INTEGER NOT NULL,
     "Name" TEXT NOT NULL,
     "UserId" INTEGER NOT NULL,
     "CreatedAt" TEXT NOT NULL,
     CONSTRAINT "FK_User" FOREIGN KEY ("UserId") REFERENCES "AppUser" ON DELETE CASCADE
);

CREATE UNIQUE INDEX IF NOT EXISTS "IX_AppUser_Username" ON "AppUser" ("Username");
CREATE UNIQUE INDEX IF NOT EXISTS "IX_RefreshToken_Token" ON "RefreshToken" ("Token");