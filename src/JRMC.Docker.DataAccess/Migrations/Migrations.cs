using System.Reflection;
using DbUp;

namespace JRMC.Docker.DataAccess.Migrations;

public static class Migrations
{
    public static void Apply(string? connectionString)
    {
        ArgumentException.ThrowIfNullOrEmpty(connectionString);

        var migrator = DeployChanges.To
            .SqliteDatabase(connectionString)
            .WithScriptsEmbeddedInAssembly(Assembly.GetExecutingAssembly())
            .LogToConsole()
            .Build();

        var result = migrator.PerformUpgrade();

        if (!result.Successful)
        {
            throw result.Error;
        }
    }
}