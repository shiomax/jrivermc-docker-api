﻿using Dapper;
using JRMC.Docker.DataAccess.Contracts;
using JRMC.Docker.DataAccess.Entities;
using Microsoft.Data.Sqlite;

namespace JRMC.Docker.DataAccess.Repositories;

[DapperAot]
internal class UserRepository(
    IDatabaseSettings settings) : IUserRepository
{
    public async Task ChangePasswordAsync(long id, string password)
    {
        await using var connection = new SqliteConnection(settings.ConnectionString);

        await connection.ExecuteAsync(
            "UPDATE AppUser SET Password=@password WHERE Id=@id",
            new { id, password });
    }

    public async Task ChangeUsernameAsync(long id, string username)
    {
        await using var connection = new SqliteConnection(settings.ConnectionString);

        await connection.ExecuteAsync(
            "UPDATE AppUser SET Username=@username WHERE Id=@id",
            new { id, username });
    }

    public async Task<long> CreateAsync(AppUser newUser)
    {
        await using var connection = new SqliteConnection(settings.ConnectionString);

        const string sql =
            $"""
              INSERT INTO AppUser
              (Username, Password, UserRole, EnableTwoFa) VALUES
              (
                @{nameof(AppUser.Username)},
                @{nameof(AppUser.Password)},
                @{nameof(AppUser.UserRole)},
                @{nameof(AppUser.EnableTwoFa)}
              ) RETURNING *;
              """;
        
        var insertedRows = await connection.QueryAsync<AppUser>(sql, newUser);

        return insertedRows.FirstOrDefault()?.Id ?? -1;
    }

    public async Task<bool> HasTwoFactorsAsync(long userId)
    {
        await using var connection = new SqliteConnection(settings.ConnectionString);
        
        const string sql =
            $"SELECT count(*) FROM {nameof(TwoFactor)} WHERE {nameof(TwoFactor.UserId)} = @userId";
        
        var count = await connection.ExecuteScalarAsync<int>(sql, new { userId });
        return count > 0;
    }

    public async Task ChangeTwoFactorEnabledAsync(long id, bool enabled)
    {
        await using var connection = new SqliteConnection(settings.ConnectionString);

        const string sql = $"UPDATE {nameof(AppUser)} SET {nameof(AppUser.EnableTwoFa)}=@enabled WHERE Id=@id";
        
        await connection.ExecuteAsync(sql, new { id, enabled });
    }

    public async Task<AppUser?> FindAsync(string username, string? password = null)
    {
        await using var connection = new SqliteConnection(settings.ConnectionString);

        var results = password switch
        {
            null => await connection.QueryAsync<AppUser>("SELECT * FROM AppUser WHERE AppUser.Username=@username", new { username }),
            _ => await connection.QueryAsync<AppUser>("""
                                                      SELECT * FROM AppUser WHERE
                                                      Username=@username AND
                                                      Password=@password
                                                      """, new { username, password })
        };

        return results.FirstOrDefault();

    }

    public async Task<bool> IsEmptyAsync()
    {
        await using var connection = new SqliteConnection(settings.ConnectionString);

        var count = await connection.ExecuteScalarAsync<int>(
            "SELECT count(*) FROM AppUser");

        return count == 0;

    }

    public async Task<AppUser?> GetByIdAsync(long id)
    {
        await using var connection = new SqliteConnection(settings.ConnectionString);

        var results = await connection.QueryAsync<AppUser>(
            "SELECT * FROM AppUser WHERE Id=@id",
            new { id });

        return results.FirstOrDefault();

    }

    public async Task<List<AppUser>> GetListAsync()
    {
        await using var connection = new SqliteConnection(settings.ConnectionString);
        var results = await connection.QueryAsync<AppUser>("SELECT * FROM AppUser");
        return results.ToList();
    }

    public async Task<bool> DeleteAsync(long id)
    {
        await using var connection = new SqliteConnection(settings.ConnectionString);

        var affectedRows = await connection.ExecuteAsync("DELETE FROM AppUser WHERE Id = @id", new { id });
        return affectedRows >= 1;
    }

    public async Task<long> CountAsync()
    {
        await using var connection = new SqliteConnection(settings.ConnectionString);

        var count = await connection.ExecuteScalarAsync<long>("SELECT count(*) FROM AppUser");

        return count;
    }

    public async Task<List<TwoFactor>> GetTwoFactorMethodsForUserAsync(long userId)
    {
        await using var connection = new SqliteConnection(settings.ConnectionString);

        var results =
            await connection.QueryAsync<TwoFactor>(
                $"SELECT * FROM TwoFactor WHERE {nameof(TwoFactor.UserId)} = @userId", new { userId });

        return results.ToList();
    }

    public async Task<long> CreateTwoFactorMethodAsync(TwoFactor twoFactor)
    {
        await using var connection = new SqliteConnection(settings.ConnectionString);
        twoFactor.CreatedAt = DateTime.UtcNow;

        const string sql =
            $"""
             INSERT INTO TwoFactor
             (Name, TwoFactorMethod, CreatedAt, Secret, UserId) VALUES
             (
               @{nameof(TwoFactor.Name)},
               @{nameof(TwoFactor.TwoFactorMethod)},
               @{nameof(TwoFactor.CreatedAt)},
               @{nameof(TwoFactor.Secret)},
               @{nameof(TwoFactor.UserId)}
             ) RETURNING *;
             """;
        
        var insertedRows = await connection.QueryAsync<TwoFactor>(sql, twoFactor);

        return insertedRows.FirstOrDefault()?.Id ?? -1;
    }

    public async Task<TwoFactor?> GetTwoFactorById(long id)
    {
        await using var connection = new SqliteConnection(settings.ConnectionString);

        var results = await connection.QueryAsync<TwoFactor>($"SELECT * FROM TwoFactor WHERE {nameof(TwoFactor.Id)} = @id", new { id });

        return results.FirstOrDefault();
    }

    public async Task<bool> DeleteTwoFactorAsync(long id)
    {
        await using var connection = new SqliteConnection(settings.ConnectionString);

        var affectedRows = await connection.ExecuteAsync($"DELETE FROM TwoFactor WHERE {nameof(TwoFactor.Id)} = @id", new { id });
        
        return affectedRows >= 1;
    }
}
