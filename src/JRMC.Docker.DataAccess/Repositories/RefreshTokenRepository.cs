﻿using Dapper;
using JRMC.Docker.DataAccess.Contracts;
using JRMC.Docker.DataAccess.Entities;
using Microsoft.Data.Sqlite;

namespace JRMC.Docker.DataAccess.Repositories;

[DapperAot]
internal class RefreshTokenRepository(
    IDatabaseSettings settings) : IRefreshTokenRepository
{
    public async Task<long> CreateAsync(RefreshToken refreshToken)
    {
        await using var connection = new SqliteConnection(settings.ConnectionString);
        
        const string sql =
            $"""
              INSERT INTO RefreshToken
              (UserId, JwtId, ExpiryTime, CreationTime, Token) VALUES
              (
                @{nameof(RefreshToken.UserId)},
                @{nameof(RefreshToken.JwtId)},
                @{nameof(RefreshToken.ExpiryTime)},
                @{nameof(RefreshToken.CreationTime)},
                @{nameof(RefreshToken.Token)}
              ) RETURNING *;
              """ ;

        var insertedRows = await connection.QueryAsync<RefreshToken>(sql, refreshToken);
        return insertedRows?.FirstOrDefault()?.Id ?? -1;
    }

    public async Task<RefreshToken?> GetByIdAsync(long id)
    {
        await using var connection = new SqliteConnection(settings.ConnectionString);

        var results = await connection.QueryAsync<RefreshToken>(
            "SELECT * FROM RefreshToken WHERE Id=@id",
            new { id });

        return results?.FirstOrDefault();

    }

    public async Task<RefreshToken?> FindAsync(string refreshToken)
    {
        await using var connection = new SqliteConnection(settings.ConnectionString);

        var results = await connection.QueryAsync<RefreshToken>(
            "SELECT * FROM RefreshToken WHERE Token=@refreshToken",
            new { refreshToken });

        return results?.FirstOrDefault();

    }

    public async Task<int> DeleteAllExpiredAsync(DateTime time)
    {
        await using var connection = new SqliteConnection(settings.ConnectionString);

        var affectedRows = await connection.ExecuteAsync($"DELETE FROM RefreshToken WHERE ExpiryTime < @time", new { time });
        
        return affectedRows;
    }

    public async Task<bool> DeleteAsync(long id)
    {
        await using var connection = new SqliteConnection(settings.ConnectionString);

        var affectedRows = await connection.ExecuteAsync(
            "DELETE FROM RefreshToken WHERE Id = @id",
            new { id });

        return affectedRows == 1;
    }

    public async Task<int> ClearAllAsync() 
    {
        await using var connection = new SqliteConnection(settings.ConnectionString);

        var affectedRows = await connection.ExecuteAsync("DELETE FROM RefreshToken");

        return affectedRows;

    }
}
