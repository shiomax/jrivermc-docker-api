﻿namespace JRMC.Docker.DataAccess.Contracts;

public interface IDatabaseSettings
{
    string ConnectionString { get; }
}
