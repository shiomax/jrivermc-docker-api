﻿using JRMC.Docker.DataAccess.Entities;

namespace JRMC.Docker.DataAccess.Contracts;

public interface IUserRepository
{
    Task<bool> IsEmptyAsync();

    Task<AppUser?> FindAsync(string username, string? password = null);

    Task<long> CreateAsync(AppUser newUser);

    Task ChangePasswordAsync(long id, string password);

    Task ChangeUsernameAsync(long id, string username);

    Task<AppUser?> GetByIdAsync(long id);

    Task<List<AppUser>> GetListAsync();

    Task<bool> DeleteAsync(long id);

    Task<long> CountAsync();

    Task<List<TwoFactor>> GetTwoFactorMethodsForUserAsync(long userId);

    Task<long> CreateTwoFactorMethodAsync(TwoFactor twoFactor);

    Task<TwoFactor?> GetTwoFactorById(long id);

    Task<bool> DeleteTwoFactorAsync(long id);

    Task<bool> HasTwoFactorsAsync(long userId);

    Task ChangeTwoFactorEnabledAsync(long id, bool enabled);
}
