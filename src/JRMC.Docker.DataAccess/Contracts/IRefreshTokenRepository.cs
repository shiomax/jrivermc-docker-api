﻿using JRMC.Docker.DataAccess.Entities;

namespace JRMC.Docker.DataAccess.Contracts
{
    public interface IRefreshTokenRepository
    {
        Task<long> CreateAsync(RefreshToken refreshToken);

        Task<RefreshToken?> GetByIdAsync(long id);

        Task<RefreshToken?> FindAsync(string refreshToken);

        Task<bool> DeleteAsync(long id);

        Task<int> DeleteAllExpiredAsync(DateTime time);

        Task<int> ClearAllAsync();
    }
}
