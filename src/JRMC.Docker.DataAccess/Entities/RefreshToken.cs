﻿using JRMC.Docker.DataAccess.Entities.Base;

namespace JRMC.Docker.DataAccess.Entities;

public class RefreshToken : IEntityBase
{
    public long Id { get; set; }
    
    public string Token { get; set; }
        = string.Empty;

    public string JwtId { get; set; } 
        = string.Empty;

    public DateTime CreationTime { get; set; }

    public DateTime ExpiryTime { get; set; }
    
    public long UserId { get; set; }
}
