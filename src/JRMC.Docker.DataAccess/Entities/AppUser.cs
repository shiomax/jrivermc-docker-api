﻿using System.Text.Json.Serialization;
using JRMC.Docker.DataAccess.Entities.Base;

namespace JRMC.Docker.DataAccess.Entities;

public class AppUser : IEntityBase
{
    public long Id { get; set; }
    
    public string Username { get; set; } 
        = string.Empty;

    public string Password { get; set; } 
        = string.Empty;

    public int UserRole { get; set; } 
        = (int) Entities.UserRole.Member;

    public bool EnableTwoFa { get; set; }
        = false;
}

[JsonConverter(typeof(JsonStringEnumConverter<UserRole>))]
public enum UserRole
{
    Owner = 1,
    Admin = 2,
    Member = 3
}
