using JRMC.Docker.DataAccess.Entities.Base;

namespace JRMC.Docker.DataAccess.Entities;

public class TwoFactor : IEntityBase
{
    public long Id { get; set; }
    
    public byte[] Secret { get; set; }
        = Array.Empty<byte>();

    public int TwoFactorMethod { get; set; }

    public string Name { get; set; }
        = string.Empty;

    public long UserId { get; set; }

    public DateTime CreatedAt { get; set; }
}

public enum TwoFactorMethod
{
    Totp = 1
}