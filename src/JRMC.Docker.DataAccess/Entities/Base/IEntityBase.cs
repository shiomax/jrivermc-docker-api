﻿namespace JRMC.Docker.DataAccess.Entities.Base
{
    public interface IEntityBase
    {
        public long Id { get; set; }
    }
}
