using System.Text.Json.Serialization;

namespace JRMC.Docker.WebApi.Contracts.Responses.General;

public class GetVersionResponse
{
    [JsonPropertyName("imageVersion")] 
    public string ImageVersion { get; set; } = string.Empty;

    [JsonPropertyName("debUrl")]
    public string DebUrl { get; set; } = string.Empty;

    [JsonPropertyName("usesRepository")]
    public bool UsesRepository { get; set; }

    [JsonPropertyName("mediaCenterTag")]
    public string MediaCenterTag { get; set; } = string.Empty;
}