using System.Text.Json.Serialization;

namespace JRMC.Docker.WebApi.Contracts.Responses.General;

public class GetInitResponse
{
    [JsonPropertyName("authRequired")]
    public bool AuthRequired { get; set; }
    
    [JsonPropertyName("canAdopt")]
    public bool CanAdopt { get; set; }

    [JsonPropertyName("initialWidth")]
    public int InitialWidth { get; set; }

    [JsonPropertyName("initialHeight")]
    public int InitialHeight { get; set; }
}