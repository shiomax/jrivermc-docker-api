﻿using System.Text.Json.Serialization;

namespace JRMC.Docker.WebApi.Contracts.Responses.Auth;

public class LoginResponse : SuccessResponse
{
    [JsonPropertyName("token")]
    public string? Token { get; set; }

    [JsonPropertyName("refreshToken")]
    public string? RefreshToken { get; set; }

    [JsonPropertyName("twoFactorRequired")]
    public bool TwoFactorRequired { get; set; }
}
