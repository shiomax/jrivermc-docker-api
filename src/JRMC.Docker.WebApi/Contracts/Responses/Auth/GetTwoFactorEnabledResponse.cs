using System.Text.Json.Serialization;

namespace JRMC.Docker.WebApi.Contracts.Responses.Auth;

public class GetTwoFactorEnabledResponse
{
    [JsonPropertyName("enabledGlobal")]
    public bool EnabledGlobal { get; set; }

    [JsonPropertyName("enabled")]
    public bool Enabled { get; set; }
}