using System.Text.Json.Serialization;

namespace JRMC.Docker.WebApi.Contracts.Responses.Auth;

public class PostCreateTwoFactorResponse : SuccessResponse
{
    [JsonPropertyName("secretBase32")] 
    public string SecretBase32 { get; set; } = string.Empty;
    
    [JsonPropertyName("totpKeyUri")] 
    public string TotpKeyUri { get; set; } = string.Empty;
}