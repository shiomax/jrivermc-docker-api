using System.Text.Json.Serialization;
using JRMC.Docker.DataAccess.Entities;

namespace JRMC.Docker.WebApi.Contracts.Responses.Auth;

public class GetTwoFactorResponse
{
    [JsonPropertyName("factors")]
    public IEnumerable<GetTwoFactorResponseItem> Factors { get; set; }
        = Enumerable.Empty<GetTwoFactorResponseItem>();
}

public class GetTwoFactorResponseItem
{
    [JsonPropertyName("id")]
    public long Id { get; set; }
    
    [JsonPropertyName("twoFactorMethod")]
    [JsonConverter(typeof(JsonStringEnumConverter<TwoFactorMethod>))]
    public TwoFactorMethod TwoFactorMethod { get; set; }

    [JsonPropertyName("name")]
    public string Name { get; set; }
        = string.Empty;

    [JsonPropertyName("createdAt")]
    public DateTime CreatedAt { get; set; }
}