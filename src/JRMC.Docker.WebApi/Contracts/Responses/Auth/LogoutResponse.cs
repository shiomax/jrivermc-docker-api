﻿namespace JRMC.Docker.WebApi.Contracts.Responses.Auth;

public class LogoutResponse : SuccessResponse
{
    public static LogoutResponse Failure(params string[] errors)
    {
        return new LogoutResponse()
        {
            Success = false,
            Errors = errors
        };
    }
}