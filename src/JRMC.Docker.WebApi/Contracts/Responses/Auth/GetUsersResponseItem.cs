using System.Text.Json.Serialization;
using JRMC.Docker.DataAccess.Entities;

namespace JRMC.Docker.WebApi.Contracts.Responses.Auth;

public class GetUsersResponse
{
    [JsonPropertyName("users")]
    public IEnumerable<GetUsersResponseItem> Users { get; set; } 
        = Enumerable.Empty<GetUsersResponseItem>();
}

public class GetUsersResponseItem
{
    [JsonPropertyName("id")]
    public long Id { get; set; }

    [JsonPropertyName("username")]
    public string Username { get; set; } = string.Empty;

    [JsonPropertyName("role")]
    [JsonConverter(typeof(JsonStringEnumConverter<UserRole>))]
    public UserRole UserRole { get; set; } = UserRole.Member;

    [JsonPropertyName("twoFactor")]
    public bool TwoFactor { get; set; }
}