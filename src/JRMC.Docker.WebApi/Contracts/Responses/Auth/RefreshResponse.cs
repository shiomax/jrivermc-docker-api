﻿using System.Text.Json.Serialization;

namespace JRMC.Docker.WebApi.Contracts.Responses.Auth;

public class RefreshResponse : SuccessResponse
{
    [JsonPropertyName("token")]
    public string? Token { get; set; }

    [JsonPropertyName("refreshToken")]
    public string? RefreshToken { get; set; }

    public static RefreshResponse Failure(params string[] errors)
    {
        return new RefreshResponse()
        {
            Success = false,
            Errors = errors
        };
    }
}
