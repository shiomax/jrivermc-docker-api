﻿using System.Text.Json.Serialization;

namespace JRMC.Docker.WebApi.Contracts.Responses.Activation;

public class ActivationStatusResponse : SuccessResponse
{
    [JsonPropertyName("isRunning")]
    public bool IsRunning { get; set; }

    [JsonPropertyName("exitCode")] 
    public int? ExitCode { get; set; }

    [JsonPropertyName("output")]
    public IEnumerable<string> Output { get; set; }
        = Enumerable.Empty<string>();
}
