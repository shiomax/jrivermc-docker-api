﻿using System.Text.Json.Serialization;

namespace JRMC.Docker.WebApi.Contracts.Responses;

public abstract class SuccessResponse
{
    [JsonPropertyName("success")]
    public bool Success { get; set; }

    [JsonPropertyName("errors")]
    public IEnumerable<string> Errors { get; set; }
        = Enumerable.Empty<string>();
}
