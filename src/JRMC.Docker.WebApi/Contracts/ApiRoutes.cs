﻿namespace JRMC.Docker.WebApi.Contracts;

public static class ApiRoutes
{
    public static class Auth
    {
        public const string PostLogin = "api/auth/login";

        public const string PostLogout = "api/auth/logout";

        public const string PostRefresh = "api/auth/refresh";

        public const string PostAdopt = "api/auth/adopt";

        public const string PostCreateUser = "api/auth/createUser";

        public const string PostChangePassword = "api/auth/changePassword";

        public const string PostChangeUsername = "api/auth/changeUsername";

        public const string GetUsers = "api/auth/users";

        public const string DeleteUser = "api/auth/users/{id}";

        public const string GetTwoFactors = "api/auth/twofactor";
        
        public const string PostCreateTwoFactor = "api/auth/twofactor";

        public const string DeleteTwoFactor = "api/auth/twofactor/{id}";

        public const string GetTwoFactorEnabled = "api/auth/twofactor/enabled";

        public const string PostTwoFactorEnabled = "api/auth/twofactor/enabled";
    }

    public static class Activation
    {
        public const string PostActivateMjrFile = "api/activation/mjr";

        public const string GetActivationResponse = "api/activation/mjr/status";
    }

    public static class General
    {
        public const string GetVersion = "api/general/version";
        
        public const string GetInit = "api/general/init";
    }
}
