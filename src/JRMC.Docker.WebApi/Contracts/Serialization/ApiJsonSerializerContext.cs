using System.Text.Json.Serialization;
using JRMC.Docker.WebApi.Contracts.Requests.Auth;
using JRMC.Docker.WebApi.Contracts.Responses.Activation;
using JRMC.Docker.WebApi.Contracts.Responses.Auth;
using JRMC.Docker.WebApi.Contracts.Responses.General;

namespace JRMC.Docker.WebApi.Contracts.Serialization;

[JsonSourceGenerationOptions(
    GenerationMode = JsonSourceGenerationMode.Default, 
    WriteIndented = false,
    IncludeFields = false,
    IgnoreReadOnlyFields = false,
    IgnoreReadOnlyProperties = false,
    PropertyNamingPolicy = JsonKnownNamingPolicy.Unspecified)]
// Activation Responses
[JsonSerializable(typeof(ActivationStatusResponse))]
[JsonSerializable(typeof(PostActivateMjrFileResponse))]
// Auth Requests
[JsonSerializable(typeof(AdoptRequest))]
[JsonSerializable(typeof(ChangePasswordRequest))]
[JsonSerializable(typeof(ChangeUsernameRequest))]
[JsonSerializable(typeof(CreateUserRequest))]
[JsonSerializable(typeof(LoginRequest))]
[JsonSerializable(typeof(LogoutRequest))]
[JsonSerializable(typeof(RefreshRequest))]
[JsonSerializable(typeof(PostCreateTwoFactorRequest))]
[JsonSerializable(typeof(PostTwoFactorEnabledRequest))]
// Auth Responses
[JsonSerializable(typeof(AdoptResponse))]
[JsonSerializable(typeof(ChangePasswordResponse))]
[JsonSerializable(typeof(ChangeUsernameResponse))]
[JsonSerializable(typeof(CreateUserResponse))]
[JsonSerializable(typeof(LoginResponse))]
[JsonSerializable(typeof(LogoutResponse))]
[JsonSerializable(typeof(RefreshResponse))]
[JsonSerializable(typeof(GetUsersResponse))]
[JsonSerializable(typeof(DeleteUserResponse))]
[JsonSerializable(typeof(GetTwoFactorResponse))]
[JsonSerializable(typeof(PostCreateTwoFactorResponse))]
[JsonSerializable(typeof(DeleteTwoFactorResponse))]
[JsonSerializable(typeof(GetTwoFactorEnabledResponse))]
[JsonSerializable(typeof(PostTwoFactorEnabledResponse))]
// General Responses
[JsonSerializable(typeof(GetInitResponse))]
[JsonSerializable(typeof(GetVersionResponse))]
internal partial class ApiJsonSerializerContext : JsonSerializerContext
{
}