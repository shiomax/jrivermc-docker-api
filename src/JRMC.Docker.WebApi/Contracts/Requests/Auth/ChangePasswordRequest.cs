﻿using System.Text.Json.Serialization;

namespace JRMC.Docker.WebApi.Contracts.Requests.Auth;

public class ChangePasswordRequest
{
    [JsonPropertyName("password")]
    public string Password { get; set; } = string.Empty;

    [JsonPropertyName("newPassword")]
    public string NewPassword { get; set; } = string.Empty;
}
