﻿using System.Text.Json.Serialization;

namespace JRMC.Docker.WebApi.Contracts.Requests.Auth;

public class ChangeUsernameRequest
{
    [JsonPropertyName("password")]
    public string Password { get; set; } = string.Empty;

    [JsonPropertyName("newUsername")]
    public string NewUsername { get; set; } = string.Empty;
}
