﻿using System.Text.Json.Serialization;

namespace JRMC.Docker.WebApi.Contracts.Requests.Auth;

public class LogoutRequest
{
    [JsonPropertyName("token")]
    public string Token { get; set; }
        = string.Empty;

    [JsonPropertyName("refreshToken")]
    public string RefreshToken { get; set; }
        = string.Empty;
}