using System.Text.Json.Serialization;

namespace JRMC.Docker.WebApi.Contracts.Requests.Auth;

public class PostTwoFactorEnabledRequest
{
    [JsonPropertyName("enabled")]
    public bool Enabled { get; set; }
}