﻿using System.Text.Json.Serialization;

namespace JRMC.Docker.WebApi.Contracts.Requests.Auth;

public class AdoptRequest
{
    [JsonPropertyName("username")]
    public string Username { get; set; } = string.Empty;

    [JsonPropertyName("password")]
    public string Password { get; set; } = string.Empty;
}
