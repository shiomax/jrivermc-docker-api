﻿using JRMC.Docker.DataAccess.Entities;
using System.Text.Json.Serialization;

namespace JRMC.Docker.WebApi.Contracts.Requests.Auth;

public class CreateUserRequest
{
    [JsonPropertyName("username")]
    public string Username { get; set; } = string.Empty;

    [JsonPropertyName("password")]
    public string Password { get; set; } = string.Empty;

    [JsonPropertyName("role")]
    [JsonConverter(typeof(JsonStringEnumConverter<UserRole>))]
    public UserRole UserRole { get; set; }
}
