using System.Text.Json.Serialization;

namespace JRMC.Docker.WebApi.Contracts.Requests.Auth;

public class PostCreateTwoFactorRequest
{
    [JsonPropertyName("name")] 
    public string Name { get; set; } = string.Empty;
}