using JRMC.Docker.WebApi.Contracts;
using JRMC.Docker.WebApi.Services.Activation.Contracts;
using Microsoft.AspNetCore.Mvc;

namespace JRMC.Docker.WebApi.Endpoints.Activation;

public abstract class GetActivationStatus : IRegisterEndpoint
{
    public static void Register(WebApplication app)
    {
        app.MapGet(ApiRoutes.Activation.GetActivationResponse, 
            ([FromServices] IActivationService activationService) => activationService.Status);
    }
}