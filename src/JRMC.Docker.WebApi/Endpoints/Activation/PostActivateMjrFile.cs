using JRMC.Docker.WebApi.Contracts;
using JRMC.Docker.WebApi.Contracts.Responses.Activation;
using JRMC.Docker.WebApi.Services.Activation.Contracts;
using Microsoft.AspNetCore.Mvc;

namespace JRMC.Docker.WebApi.Endpoints.Activation;

public abstract class PostActivateMjrFile : IRegisterEndpoint
{
    private static readonly string[] ErrorHttpHeader = [ "Use content-type to be 'multipart/form-data'" ];
    private static readonly string[] ErrorMjrFileRequired = [ "MJR File is required." ];

    private static async Task<IResult> HandleRequest(
        [FromServices] IActivationService activationService,
        HttpRequest request)
    {
        if (request.ContentType is null || !request.ContentType.Contains("multipart/form-data"))
        {
            return Results.Ok(new PostActivateMjrFileResponse()
            {
                Success = false,
                Errors = ErrorHttpHeader
            });
        }

        if (request.Form.Files.Count < 1 || request.Form.Files[0].Length <= 0) 
            return Results.Ok(new PostActivateMjrFileResponse() { 
                Success = false,
                Errors = ErrorMjrFileRequired
            });

        var file = request.Form.Files[0];
        var response = await activationService.ActivateJRiverAsync(file);
        
        return Results.Ok(response);
    }
    
    public static void Register(WebApplication app)
    {
        app.MapPost(ApiRoutes.Activation.PostActivateMjrFile, HandleRequest);
    }
}