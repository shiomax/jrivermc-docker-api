using JRMC.Docker.DataAccess.Contracts;
using JRMC.Docker.DataAccess.Entities;
using JRMC.Docker.WebApi.Contracts;
using JRMC.Docker.WebApi.Contracts.Responses.Auth;
using JRMC.Docker.WebApi.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace JRMC.Docker.WebApi.Endpoints.Auth;

public abstract class DeleteUser : IRegisterEndpoint
{
    private static readonly string[] ErrorOwnerCannotBeDeleted = ["Owner cannot be deleted."];
    private static readonly string[] ErrorUserDoesNotExist = ["User does not exist."];
    private static readonly string[] ErrorCannotDeleteUserWithHigherPrivileges = ["You cannot delete a User with higher privileges than your own."];
    private static readonly string[] ErrorCannotDeleteUser = ["Cannot delete last User."];
    private static readonly string[] ErrorUserCouldNotDeleteUser = ["User could not be deleted."];

    [Authorize(Roles = "Owner, Admin")]
    private static async Task<IResult> HandleRequest(
        HttpContext context,
        [FromRoute] long id,
        [FromServices] IUserRepository userRepository)
    {
        var userId = context.GetUserId();
        var userRole = context.GetUserRole();

        if (userId == id && userRole == UserRole.Owner)
        {
            return Results.Ok(new DeleteUserResponse()
            {
                Success = false,
                Errors = ErrorOwnerCannotBeDeleted
            });
        }

        var user = await userRepository.GetByIdAsync(id);

        if (user is null)
        {
            return Results.Ok(new DeleteUserResponse()
            {
                Success = false,
                Errors = ErrorUserDoesNotExist
            });
        }
        
        if ((int)userRole > (int)user.UserRole)
        {
            return Results.Ok(new DeleteUserResponse()
            {
                Success = false,
                Errors = ErrorCannotDeleteUserWithHigherPrivileges
            });
        }

        // Theoretically should not be possible because owner cannot be deleted, checking anyways
        var count = await userRepository.CountAsync();
        if (count == 1)
        {
            return Results.Ok(new DeleteUserResponse()
            {
                Success = false,
                Errors = ErrorCannotDeleteUser
            });
        }
        
        var deleted = await userRepository.DeleteAsync(id);

        if (!deleted)
        {
            return Results.Ok(new DeleteUserResponse()
            {
                Success = false,
                Errors = ErrorUserCouldNotDeleteUser
            });
        }

        return Results.Ok(new DeleteUserResponse()
        {
            Success = true
        });
    }
    
    
    public static void Register(WebApplication app)
    {
        app.MapDelete(ApiRoutes.Auth.DeleteUser, HandleRequest);
    }
}