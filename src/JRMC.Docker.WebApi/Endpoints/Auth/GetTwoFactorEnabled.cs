using JRMC.Docker.DataAccess.Contracts;
using JRMC.Docker.WebApi.Contracts;
using JRMC.Docker.WebApi.Contracts.Responses.Auth;
using JRMC.Docker.WebApi.Extensions;
using JRMC.Docker.WebApi.Settings;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace JRMC.Docker.WebApi.Endpoints.Auth;

public abstract class GetTwoFactorEnabled : IRegisterEndpoint
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    private static async Task<IResult> HandleRequest(
        HttpContext context,
        [FromServices] IUserRepository repository,
        [FromServices] TwoFactorSettings settings)
    {
        var userId = context.GetUserId();
        var user = await repository.GetByIdAsync(userId);

        if (user is null) return Results.NotFound();

        return Results.Ok(new GetTwoFactorEnabledResponse()
        {
            Enabled = user.EnableTwoFa,
            EnabledGlobal = settings.Enabled
        });
    }
    
    public static void Register(WebApplication app)
    {
        app.MapGet(ApiRoutes.Auth.GetTwoFactorEnabled, HandleRequest);
    }
}