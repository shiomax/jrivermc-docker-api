using JRMC.Docker.DataAccess.Contracts;
using JRMC.Docker.DataAccess.Entities;
using JRMC.Docker.WebApi.Contracts;
using JRMC.Docker.WebApi.Contracts.Requests.Auth;
using JRMC.Docker.WebApi.Contracts.Responses.Auth;
using JRMC.Docker.WebApi.Services.Auth.Contracts;
using JRMC.Docker.WebApi.Settings;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace JRMC.Docker.WebApi.Endpoints.Auth;

public abstract class PostAdopt : IRegisterEndpoint
{
    private static readonly SemaphoreSlim AdoptSemaphore = new(1, 1);
    private static readonly string[] ErrorAuthDisabled = ["Authentication is disabled."];
    private static readonly string[] ErrorInstanceAlreadyAdopted = ["Instance already adopted."];

    [AllowAnonymous]
    private static async Task<IResult> HandleRequest(
        [FromBody] AdoptRequest request,
        [FromServices] IUserRepository userRepository,
        [FromServices] IAuthService authService,
        [FromServices] JwtSettings settings)
    {
        if (settings is not { Enabled: true })
        {
            return Results.Ok(new AdoptResponse()
            {
                Success = false,
                Errors = ErrorAuthDisabled
            });
        }
        
        await AdoptSemaphore.WaitAsync();

        try
        {
            var isEmpty = await userRepository.IsEmptyAsync();

            if (!isEmpty)
            {
                return Results.Ok(new AdoptResponse()
                {
                    Success = false,
                    Errors = ErrorInstanceAlreadyAdopted
                });
            }

            var result = await authService.RegisterAsync(
                request.Username, request.Password, UserRole.Owner);

            return Results.Ok(new AdoptResponse()
            {
                Success = result.IsSucessful,
                Errors = result.Errors
            });
        }
        finally
        {
            AdoptSemaphore.Release();
        }
    }
    
    public static void Register(WebApplication app)
    {
        app.MapPost(ApiRoutes.Auth.PostAdopt, HandleRequest);
    }
}