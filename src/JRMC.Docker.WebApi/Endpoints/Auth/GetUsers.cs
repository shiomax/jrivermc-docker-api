using JRMC.Docker.DataAccess.Contracts;
using JRMC.Docker.DataAccess.Entities;
using JRMC.Docker.WebApi.Contracts;
using JRMC.Docker.WebApi.Contracts.Responses.Auth;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace JRMC.Docker.WebApi.Endpoints.Auth;

public abstract class GetUsers : IRegisterEndpoint
{
    [Authorize(Roles = "Owner, Admin")]
    private static async Task<IResult> HandleRequest(
        [FromServices] IUserRepository repository)
    {
        var users = await repository.GetListAsync();

        var response = new GetUsersResponse()
        {
            Users = users.Select(x => new GetUsersResponseItem()
            {
                Id = x.Id,
                UserRole = (UserRole) x.UserRole,
                Username = x.Username,
                TwoFactor = x.EnableTwoFa
            })
        };

        return Results.Ok(response);
    }
    
    
    public static void Register(WebApplication app)
    {
        app.MapGet(ApiRoutes.Auth.GetUsers, HandleRequest);
    }
}