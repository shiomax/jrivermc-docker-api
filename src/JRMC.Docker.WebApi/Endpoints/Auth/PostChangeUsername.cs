using JRMC.Docker.WebApi.Contracts;
using JRMC.Docker.WebApi.Contracts.Requests.Auth;
using JRMC.Docker.WebApi.Contracts.Responses.Auth;
using JRMC.Docker.WebApi.Services.Auth.Contracts;
using JRMC.Docker.WebApi.Settings;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using JRMC.Docker.WebApi.Extensions;

namespace JRMC.Docker.WebApi.Endpoints.Auth;

public abstract class PostChangeUsername : IRegisterEndpoint
{
    private static readonly string[] AuthenticationDisabledError = ["Authentication is disabled."];

    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    private static async Task<IResult> HandleRequest(
        HttpContext context,
        [FromBody] ChangeUsernameRequest request,
        [FromServices] IAuthService authService,
        [FromServices] JwtSettings settings)
    {
        if (settings is not { Enabled: true })
        {
            return Results.Ok(new ChangeUsernameResponse()
            {
                Success = false,
                Errors = AuthenticationDisabledError
            });
        }
        
        var userId = context.GetUserId();

        return Results.Ok(await authService.ChangeUsernameAsync(
            userId, request.Password, request.NewUsername));
    }
    
    public static void Register(WebApplication app)
    {
        app.MapPost(ApiRoutes.Auth.PostChangeUsername, HandleRequest);
    }
}