using JRMC.Docker.DataAccess.Contracts;
using JRMC.Docker.WebApi.Contracts;
using JRMC.Docker.WebApi.Contracts.Responses.Auth;
using JRMC.Docker.WebApi.Extensions;
using JRMC.Docker.WebApi.Settings;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace JRMC.Docker.WebApi.Endpoints.Auth;

public abstract class DeleteTwoFactor : IRegisterEndpoint
{
    private static readonly string[] ErrorAuthDisabled = ["Authentication is disabled."];
    private static readonly string[] ErrorTwoFactorDoesNotExist = ["Two Factor does not exist"];
    private static readonly string[] ErrorCannotDeleteForOtherUsers = ["Cannot delete Two Factors for other users"];
    private static readonly string[] ErrorCouldNotDeleteTwoFactor = ["Could not delete Two Factor"];

    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    private static async Task<IResult> HandleRequest(
        HttpContext context,
        [FromRoute] long id,
        [FromServices] IUserRepository userRepository,
        [FromServices] JwtSettings settings)
    {
        if (settings is not { Enabled: true })
        {
            return Results.Ok(new DeleteTwoFactorResponse()
            {
                Success = false,
                Errors = ErrorAuthDisabled
            });
        }
        
        var userId = context.GetUserId();
        var twoFactor = await userRepository.GetTwoFactorById(id);

        if (twoFactor is null)
            return Results.Ok(new DeleteTwoFactorResponse()
            {
                Success = false,
                Errors = ErrorTwoFactorDoesNotExist
            });

        if (twoFactor.UserId != userId)
            return Results.Ok(new DeleteTwoFactorResponse()
            {
                Success = false,
                Errors = ErrorCannotDeleteForOtherUsers
            });

        var success = await userRepository.DeleteTwoFactorAsync(id);

        if (!success)
            return Results.Ok(new DeleteTwoFactorResponse()
            {
                Success = false,
                Errors = ErrorCouldNotDeleteTwoFactor
            });

        // Disable Two Factor Authentication when last one has been removed
        if (!await userRepository.HasTwoFactorsAsync(userId))
            await userRepository.ChangeTwoFactorEnabledAsync(userId, false);

        return Results.Ok(new DeleteTwoFactorResponse()
        {
            Success = true
        });
    }

    public static void Register(WebApplication app)
    {
        app.MapDelete(ApiRoutes.Auth.DeleteTwoFactor, HandleRequest);
    }
}