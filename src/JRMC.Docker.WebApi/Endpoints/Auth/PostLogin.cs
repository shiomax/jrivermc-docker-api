using System.Net;
using JRMC.Docker.WebApi.Contracts;
using JRMC.Docker.WebApi.Contracts.Requests.Auth;
using JRMC.Docker.WebApi.Contracts.Responses.Auth;
using JRMC.Docker.WebApi.Services.Auth.Contracts;
using JRMC.Docker.WebApi.Settings;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;

namespace JRMC.Docker.WebApi.Endpoints.Auth;

public abstract class PostLogin : IRegisterEndpoint
{
    private static readonly string[] ErrorAuthDisabled = ["Authentication is disabled."];

    [AllowAnonymous]
    private static async Task<IResult> HandleRequest(
        HttpRequest httpRequest,
        [FromServices] ILogger<PostLogin> logger,
        [FromServices] IAuthService authService,
        [FromServices] JwtSettings settings,
        [FromBody] LoginRequest request)
    {
        if (!settings.Enabled)
        {
            return Results.Ok(new LoginResponse()
            {
                Success = false,
                Errors = ErrorAuthDisabled
            });
        }

        var ipAddress = httpRequest.HttpContext.Features.Get<IHttpConnectionFeature>()?.RemoteIpAddress;
        logger.LogLoginRequest(ipAddress);

        var response = await authService.LoginAsync(request.Username, request.Password, request.TwoFactorCode);

        if(!response.Success)
            logger.LogLoginFailed(ipAddress);

        return Results.Ok(response);
    }
    
    public static void Register(WebApplication app)
    {
        app.MapPost(ApiRoutes.Auth.PostLogin, HandleRequest);
    }
}

internal static partial class PostLoginLogExtensions
{
    [LoggerMessage(
        Level = LogLevel.Information,
        Message = "Login request from {IpAddress}")]
    internal static partial void LogLoginRequest(this ILogger logger, IPAddress? ipAddress);

    [LoggerMessage(
        Level = LogLevel.Information,
        Message = "Login request from {IpAddress} failed")]
    internal static partial void LogLoginFailed(this ILogger logger, IPAddress? ipAddress);
}