using JRMC.Docker.DataAccess.Contracts;
using JRMC.Docker.DataAccess.Entities;
using JRMC.Docker.WebApi.Contracts;
using JRMC.Docker.WebApi.Contracts.Responses.Auth;
using JRMC.Docker.WebApi.Extensions;
using JRMC.Docker.WebApi.Settings;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace JRMC.Docker.WebApi.Endpoints.Auth;

public abstract class GetTwoFactors : IRegisterEndpoint
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    private static async Task<IResult> HandleRequest(
        HttpContext context,
        [FromServices] IUserRepository userRepository,
        [FromServices] JwtSettings settings)
    {
        if (settings is not { Enabled: true })
        {
            return Results.Ok(new GetTwoFactorResponse()
            {
                Factors = Array.Empty<GetTwoFactorResponseItem>()
            });
        }
        
        var userId = context.GetUserId();
        var twoFactors = await userRepository.GetTwoFactorMethodsForUserAsync(userId);

        var results = twoFactors.Select(x => new GetTwoFactorResponseItem()
        {
            TwoFactorMethod = (TwoFactorMethod) x.TwoFactorMethod,
            Name = x.Name,
            Id = x.Id,
            CreatedAt = x.CreatedAt
        });
        
        return Results.Ok(new GetTwoFactorResponse()
        {
            Factors = results
        });
    }
    
    public static void Register(WebApplication app)
    {
        app.MapGet(ApiRoutes.Auth.GetTwoFactors, HandleRequest);
    }
}