using JRMC.Docker.WebApi.Contracts;
using JRMC.Docker.WebApi.Contracts.Requests.Auth;
using JRMC.Docker.WebApi.Contracts.Responses.Auth;
using JRMC.Docker.WebApi.Extensions;
using JRMC.Docker.WebApi.Services.Auth.Contracts;
using JRMC.Docker.WebApi.Settings;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace JRMC.Docker.WebApi.Endpoints.Auth;

public abstract class PostChangePassword : IRegisterEndpoint
{
    private static readonly string[] AuthenticationDisabledError = ["Authentication is disabled."];
    
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    private static async Task<IResult> HandleRequest(
        HttpContext context,
        [FromBody] ChangePasswordRequest request,
        [FromServices] IAuthService authService,
        [FromServices] JwtSettings settings)
    {
        if (settings is not { Enabled: true })
        {
            return Results.Ok(new ChangePasswordResponse()
            {
                Success = false,
                Errors = AuthenticationDisabledError
            });
        }
        
        var userId = context.GetUserId();

        return Results.Ok(await authService.ChangePasswordAsync(
            userId, request.Password, request.NewPassword));
    }
    
    public static void Register(WebApplication app)
    {
        app.MapPost(ApiRoutes.Auth.PostChangePassword, HandleRequest);
    }
}