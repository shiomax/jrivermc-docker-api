using JRMC.Docker.DataAccess.Entities;
using JRMC.Docker.WebApi.Contracts;
using JRMC.Docker.WebApi.Contracts.Requests.Auth;
using JRMC.Docker.WebApi.Contracts.Responses.Auth;
using JRMC.Docker.WebApi.Services.Auth.Contracts;
using JRMC.Docker.WebApi.Settings;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace JRMC.Docker.WebApi.Endpoints.Auth;

public abstract class PostCreateUser : IRegisterEndpoint
{
    private static readonly string[] ErrorAuthDisabled = ["Authentication is disabled."];
    private static readonly string[] ErrorCannotCreateSecondOwner = ["Cannot create a second Owner."];

    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme,
        Roles = "Owner,Admin")]
    private static async Task<IResult> HandleRequest(
        [FromBody] CreateUserRequest request,
        [FromServices] JwtSettings settings,
        [FromServices] IAuthService authService)
    {
        if (settings is not { Enabled: true })
        {
            return Results.Ok(new CreateUserResponse()
            {
                Success = false,
                Errors = ErrorAuthDisabled
            });
        }
        
        if (request.UserRole == UserRole.Owner)
            return Results.Ok(new CreateUserResponse()
            {
                Success = false,
                Errors = ErrorCannotCreateSecondOwner
            });

        var response = await authService.RegisterAsync(
            request.Username, request.Password, request.UserRole);
        return Results.Ok(new CreateUserResponse()
        {
            Success = response.IsSucessful,
            Errors = response.Errors
        });
    }
    
    public static void Register(WebApplication app)
    {
        app.MapPost(ApiRoutes.Auth.PostCreateUser, HandleRequest);
    }
}