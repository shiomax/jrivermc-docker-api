using JRMC.Docker.WebApi.Contracts;
using JRMC.Docker.WebApi.Contracts.Requests.Auth;
using JRMC.Docker.WebApi.Contracts.Responses.Auth;
using JRMC.Docker.WebApi.Services.Auth.Contracts;
using JRMC.Docker.WebApi.Settings;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace JRMC.Docker.WebApi.Endpoints.Auth;

public abstract class PostLogout : IRegisterEndpoint
{
    private static readonly string[] ErrorAuthDisabled = ["Authentication is disabled."];

    [AllowAnonymous]
    private static async Task<IResult> HandleRequest(
        [FromBody] LogoutRequest request,
        [FromServices] IAuthService authService,
        [FromServices] JwtSettings settings)
    {
        if (!settings.Enabled)
        {
            return Results.Ok(new LogoutResponse()
            {
                Success = false,
                Errors = ErrorAuthDisabled
            });
        }
        
        if (string.IsNullOrEmpty(request.Token))
            return Results.Ok(LogoutResponse.Failure("Jwt token not provided."));
        if (string.IsNullOrEmpty(request.RefreshToken))
            return Results.Ok(LogoutResponse.Failure("Refresh token not provided."));

        var response = await authService.LogoutAsync(request.Token, request.RefreshToken);

        return Results.Ok(response);
    }
    
    public static void Register(WebApplication app)
    {
        app.MapPost(ApiRoutes.Auth.PostLogout, HandleRequest);
    }
}