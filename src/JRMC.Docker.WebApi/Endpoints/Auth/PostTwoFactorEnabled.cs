using JRMC.Docker.DataAccess.Contracts;
using JRMC.Docker.WebApi.Contracts;
using JRMC.Docker.WebApi.Contracts.Requests.Auth;
using JRMC.Docker.WebApi.Contracts.Responses.Auth;
using JRMC.Docker.WebApi.Extensions;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace JRMC.Docker.WebApi.Endpoints.Auth;

public abstract class PostTwoFactorEnabled : IRegisterEndpoint
{
    private static readonly string[] ErrorUserDoesNotExist = ["User does not exist"];
    private static readonly string[] ErrorCannotEnable2Fa = ["You cannot enable 2FA without at least one configured method"];

    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    private static async Task<IResult> HandleRequest(
        HttpContext context,
        [FromBody] PostTwoFactorEnabledRequest request,
        [FromServices] IUserRepository repository)
    {
        var id = context.GetUserId();
        var user = await repository.GetByIdAsync(id);

        if(user is null) return Results.Ok(new PostTwoFactorEnabledResponse()
        {
            Success = false,
            Errors = ErrorUserDoesNotExist
        });

        if (request is { Enabled: true } && !await repository.HasTwoFactorsAsync(id))
            return Results.Ok(new PostTwoFactorEnabledResponse()
            {
                Success = false,
                Errors = ErrorCannotEnable2Fa
            });

        await repository.ChangeTwoFactorEnabledAsync(id, request.Enabled);

        return Results.Ok(new PostTwoFactorEnabledResponse() { Success = true });
    }
    
    public static void Register(WebApplication app)
    {
        app.MapPost(ApiRoutes.Auth.PostTwoFactorEnabled, HandleRequest);
    }
}