using JRMC.Docker.WebApi.Contracts;
using JRMC.Docker.WebApi.Contracts.Requests.Auth;
using JRMC.Docker.WebApi.Contracts.Responses.Auth;
using JRMC.Docker.WebApi.Services.Auth.Contracts;
using JRMC.Docker.WebApi.Settings;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace JRMC.Docker.WebApi.Endpoints.Auth;

public abstract class PostRefresh : IRegisterEndpoint
{
    private static readonly string[] ErrorAuthDisabled = ["Authentication is disabled."];

    [AllowAnonymous]
    private static async Task<IResult> HandleRequest(
        [FromBody] RefreshRequest request,
        [FromServices] JwtSettings settings,
        [FromServices] IAuthService authService)
    {
        if (settings is not { Enabled: true })
        {
            return Results.Ok(new RefreshResponse()
            {
                Success = false,
                Errors = ErrorAuthDisabled
            });
        }
        
        if (string.IsNullOrEmpty(request.Token))
            return Results.Ok(RefreshResponse.Failure("Jwt token not provided."));
        if (string.IsNullOrEmpty(request.RefreshToken))
            return Results.Ok(RefreshResponse.Failure("Refresh token not provided."));

        var response = await authService.RefreshAsync(request.Token, request.RefreshToken);
        return Results.Ok(response);
    }
    
    public static void Register(WebApplication app)
    {
        app.MapPost(ApiRoutes.Auth.PostRefresh, HandleRequest);
    }
}