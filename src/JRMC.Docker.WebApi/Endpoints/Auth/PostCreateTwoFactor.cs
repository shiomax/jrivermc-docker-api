using JRMC.Docker.DataAccess.Contracts;
using JRMC.Docker.DataAccess.Entities;
using JRMC.Docker.WebApi.Contracts;
using JRMC.Docker.WebApi.Contracts.Requests.Auth;
using JRMC.Docker.WebApi.Contracts.Responses.Auth;
using JRMC.Docker.WebApi.Extensions;
using JRMC.Docker.WebApi.Services.Auth.Contracts;
using JRMC.Docker.WebApi.Settings;
using JRMC.Docker.WebApi.Utils;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace JRMC.Docker.WebApi.Endpoints.Auth;

public abstract class PostCreateTwoFactor : IRegisterEndpoint
{
    private static readonly string[] ErrorAuthDisabled = ["Authentication is disabled."];
    private static readonly string[] ErrorNameNotProvided = ["Name not provided"];
    private static readonly string[] ErrorUserDoesNotExist = ["User does not exist"];

    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    private static async Task<IResult> HandleRequest(
        HttpContext context,
        [FromBody] PostCreateTwoFactorRequest request,
        [FromServices] ITwoFactorService twoFactorService,
        [FromServices] IUserRepository userRepository,
        [FromServices] JwtSettings settings)
    {
        if (settings is not { Enabled: true })
        {
            return Results.Ok(new PostCreateTwoFactorResponse()
            {
                Success = false,
                Errors = ErrorAuthDisabled
            });
        }
        
        if (string.IsNullOrEmpty(request.Name))
        {
            return Results.BadRequest(new PostCreateTwoFactorResponse()
            {
                Success = false,
                Errors = ErrorNameNotProvided
            });
        }
        
        var userId = context.GetUserId();
        var user = await userRepository.GetByIdAsync(userId);

        if (user is null)
        {
            return Results.BadRequest(new PostCreateTwoFactorResponse()
            {
                Success = false,
                Errors = ErrorUserDoesNotExist
            });
        }
        
        var secret = twoFactorService.GenerateSecret();

        var twoFactor = new TwoFactor()
        {
            Name = request.Name,
            Secret = secret,
            TwoFactorMethod = (int) TwoFactorMethod.Totp,
            UserId = userId
        };
        
        await userRepository.CreateTwoFactorMethodAsync(twoFactor);

        var base32Secret = Base32Encoder.Encode(secret);
        var totpUri = $"otpauth://totp/jrivermc-docker:{user.Username}?secret={Base32Encoder.Encode(secret)}";
        
        return Results.Ok(new PostCreateTwoFactorResponse()
        {
            Success = true,
            SecretBase32 = base32Secret,
            TotpKeyUri = totpUri
        });
    }
    
    public static void Register(WebApplication app)
    {
        app.MapPost(ApiRoutes.Auth.PostCreateTwoFactor, HandleRequest);
    }
}