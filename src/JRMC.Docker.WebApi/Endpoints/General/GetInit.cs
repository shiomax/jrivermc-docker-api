using JRMC.Docker.DataAccess.Contracts;
using JRMC.Docker.WebApi.Contracts;
using JRMC.Docker.WebApi.Contracts.Responses.General;
using JRMC.Docker.WebApi.Settings;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace JRMC.Docker.WebApi.Endpoints.General;

public abstract class GetInit : IRegisterEndpoint
{
    [AllowAnonymous]
    private static async Task<IResult> HandleRequest(
        [FromServices] IUserRepository userRepository,
        [FromServices] InitSettings initSettings,
        [FromServices] JwtSettings settings)
    {
        var isEmpty = settings.Enabled && await userRepository.IsEmptyAsync();

        return Results.Ok(new GetInitResponse()
        {
            AuthRequired = settings.Enabled,
            CanAdopt = isEmpty,
            InitialHeight = initSettings.InitialHeight,
            InitialWidth = initSettings.InitialWidth
        });
    }
    
    public static void Register(WebApplication app)
    {
        app.MapGet(ApiRoutes.General.GetInit, HandleRequest);
    }
}