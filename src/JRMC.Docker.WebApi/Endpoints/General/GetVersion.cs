using JRMC.Docker.WebApi.Contracts;
using JRMC.Docker.WebApi.Services.Version.Contracts;
using Microsoft.AspNetCore.Mvc;

namespace JRMC.Docker.WebApi.Endpoints.General;

public abstract class GetVersion : IRegisterEndpoint
{
    public static void Register(WebApplication app)
    {
        app.MapGet(ApiRoutes.General.GetVersion,
            async ([FromServices] IVersionService versionService) => Results.Ok(await versionService.GetVersionResponseAsync()));
    }
}