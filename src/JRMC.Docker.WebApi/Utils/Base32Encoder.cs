using System.Text;

namespace JRMC.Docker.WebApi.Utils;

/// <summary>
/// Base32 encoder used internally in Identity package
/// </summary>
public static class Base32Encoder
{
    private const string Base32Chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567";
    
    public static string Encode(byte[] input)
    {
        ArgumentNullException.ThrowIfNull(input);

        var sb = new StringBuilder();
        for (var offset = 0; offset < input.Length;)
        {
            var numCharsToOutput = GetNextGroup(input, ref offset, out var a, out var b, out var c, out var d, out var e, out var f, out var g, out var h);

            sb.Append((numCharsToOutput >= 1) ? Base32Chars[a] : '=');
            sb.Append((numCharsToOutput >= 2) ? Base32Chars[b] : '=');
            sb.Append((numCharsToOutput >= 3) ? Base32Chars[c] : '=');
            sb.Append((numCharsToOutput >= 4) ? Base32Chars[d] : '=');
            sb.Append((numCharsToOutput >= 5) ? Base32Chars[e] : '=');
            sb.Append((numCharsToOutput >= 6) ? Base32Chars[f] : '=');
            sb.Append((numCharsToOutput >= 7) ? Base32Chars[g] : '=');
            sb.Append((numCharsToOutput >= 8) ? Base32Chars[h] : '=');
        }

        return sb.ToString();
    }
    
    public static byte[] Decode(string input)
    {
        ArgumentNullException.ThrowIfNull(input);
        var trimmedInput = input.AsSpan().TrimEnd('=');
        if (trimmedInput.Length == 0)
        {
            return Array.Empty<byte>();
        }

        var output = new byte[trimmedInput.Length * 5 / 8];
        var bitIndex = 0;
        var inputIndex = 0;
        var outputBits = 0;
        var outputIndex = 0;
        while (outputIndex < output.Length)
        {
            var byteIndex = Base32Chars.IndexOf(char.ToUpperInvariant(trimmedInput[inputIndex]));
            if (byteIndex < 0)
            {
                throw new FormatException();
            }

            var bits = Math.Min(5 - bitIndex, 8 - outputBits);
            output[outputIndex] <<= bits;
            output[outputIndex] |= (byte)(byteIndex >> (5 - (bitIndex + bits)));

            bitIndex += bits;
            if (bitIndex >= 5)
            {
                inputIndex++;
                bitIndex = 0;
            }

            outputBits += bits;
            if (outputBits >= 8)
            {
                outputIndex++;
                outputBits = 0;
            }
        }
        return output;
    }
    
    private static int GetNextGroup(Span<byte> input, ref int offset, out byte a, out byte b, out byte c, out byte d, out byte e, out byte f, out byte g, out byte h)
    {
        var retVal = (input.Length - offset) switch
        {
            1 => 2,
            2 => 4,
            3 => 5,
            4 => 7,
            _ => 8
        };

        var b1 = (offset < input.Length) ? input[offset++] : 0U;
        var b2 = (offset < input.Length) ? input[offset++] : 0U;
        var b3 = (offset < input.Length) ? input[offset++] : 0U;
        var b4 = (offset < input.Length) ? input[offset++] : 0U;
        var b5 = (offset < input.Length) ? input[offset++] : 0U;

        a = (byte)(b1 >> 3);
        b = (byte)(((b1 & 0x07) << 2) | (b2 >> 6));
        c = (byte)((b2 >> 1) & 0x1f);
        d = (byte)(((b2 & 0x01) << 4) | (b3 >> 4));
        e = (byte)(((b3 & 0x0f) << 1) | (b4 >> 7));
        f = (byte)((b4 >> 2) & 0x1f);
        g = (byte)(((b4 & 0x3) << 3) | (b5 >> 5));
        h = (byte)(b5 & 0x1f);

        return retVal;
    }
}