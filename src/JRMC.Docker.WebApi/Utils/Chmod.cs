using System.Runtime.InteropServices;

namespace JRMC.Docker.WebApi.Utils;

internal class Chmod 
{
    [DllImport("libc", SetLastError = true)]
    private static extern int chmod(string pathname, int mode);

    public static int Invoke(string filename, UnixFilePermission permission)
        => chmod(Path.GetFullPath(filename), (int)permission);
}

[Flags]
internal enum UnixFilePermission
{
    // user permissions
    UserRead = 0x100,
    UserWrite = 0x80,
    UserExecute = 0x40,
    
    // group permission
    GroupRead = 0x20,
    GroupWrite = 0x10,
    GroupExecute = 0x8,
    
    // other permissions
    OthersRead = 0x4,
    OthersWrite = 0x2,
    OthersExecute = 0x1
}