﻿namespace JRMC.Docker.WebApi.Services;

public class SuccessResult
{
    public bool IsSucessful { get; private init; }

    public IEnumerable<string> Errors { get; private init; }
        = Enumerable.Empty<string>();

    public static SuccessResult Success 
        => new() { IsSucessful = true, Errors = Enumerable.Empty<string>() };

    public static SuccessResult Fail(params string[] errors) 
        => new() { IsSucessful = false, Errors = errors };
}
