using JRMC.Docker.DataAccess.Entities;

namespace JRMC.Docker.WebApi.Services.Auth.Contracts;

public interface ITwoFactorService
{
    Task<bool> ValidateTwoFactorAsync(AppUser user, string code);

    byte[] GenerateSecret();
}