﻿using JRMC.Docker.DataAccess.Entities;
using JRMC.Docker.WebApi.Contracts.Responses.Auth;

namespace JRMC.Docker.WebApi.Services.Auth.Contracts;

public interface IAuthService
{
    Task<LoginResponse> LoginAsync(string username, string password, string? twoFactorCode = null);

    Task<RefreshResponse> RefreshAsync(string jwt, string refreshToken);

    Task<LogoutResponse> LogoutAsync(string jwt, string refreshToken);

    Task<SuccessResult> RegisterAsync(string username, string password, UserRole role);

    Task<ChangePasswordResponse> ChangePasswordAsync(long id, string password, string newPassword);

    Task<ChangeUsernameResponse> ChangeUsernameAsync(long id, string password, string newUsername);
}
