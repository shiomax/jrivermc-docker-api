﻿using JRMC.Docker.DataAccess.Contracts;
using JRMC.Docker.DataAccess.Entities;
using JRMC.Docker.WebApi.Contracts.Responses.Auth;
using JRMC.Docker.WebApi.Settings;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using JRMC.Docker.WebApi.Services.Auth.Contracts;

namespace JRMC.Docker.WebApi.Services.Auth;

public class AuthService(IUserRepository userRepository,
        IRefreshTokenRepository refreshTokenRepository,
        JwtSettings jwtSettings,
        TwoFactorSettings twoFactorSettings,
        RandomNumberGenerator randomNumberGenerator,
        TokenValidationParameters tokenValidationParameters,
        ITwoFactorService twoFactorService,
        ILogger<AuthService> logger)
    : IAuthService
{
    private readonly bool twoFactorEnabled = twoFactorSettings.Enabled;

    public async Task<SuccessResult> RegisterAsync(
        string username, string password, UserRole role)
    {
        var userExists = (await userRepository.FindAsync(username)) is not null;

        if(userExists)
            return SuccessResult.Fail($"Username '{username}' already exists.");

        var user = new AppUser() { 
            Username = username,
            Password = HashPassword(password),
            UserRole = (int) role
        };

        await userRepository.CreateAsync(user);

        return SuccessResult.Success;
    }

    public async Task<LoginResponse> LoginAsync(string username, string password, string? twoFactorCode = null)
    {
        var user = await userRepository.FindAsync(username);

        if (user is null)
            return new LoginResponse() { 
                Success = false,
                Errors = new[] { $"User '{username}' does not exist." }
            };

        if (!ValidatePassword(password, user.Password))
            return new LoginResponse() { 
                Success = false,
                Errors = new[] { "Password is incorrect." }
            };

        if (twoFactorEnabled && user.EnableTwoFa)
        {
            if (string.IsNullOrEmpty(twoFactorCode))
                return new LoginResponse()
                {
                    Success = false,
                    TwoFactorRequired = true,
                    Errors = new [] { "Two Factor code is required, but not provided." }
                };

            if (!await twoFactorService.ValidateTwoFactorAsync(user, twoFactorCode))
                return new LoginResponse()
                {
                    Success = false,
                    TwoFactorRequired = true,
                    Errors = new[] { "Two Factor code is invalid." }
                };
        }
        
        var createTokenResult = await GetTokenForUserAsync(user);

        return new LoginResponse() {
            Success = true,
            Token = createTokenResult.Token,
            RefreshToken = createTokenResult.RefreshToken
        };
    }

    public async Task<RefreshResponse> RefreshAsync(string jwt, string refreshToken)
    {
        var validatedToken = GetPrincipalFromToken(jwt);

        if (validatedToken is null)
            return RefreshResponse.Failure("Jwt Token invalid.");

        var storedRefreshToken = await refreshTokenRepository.FindAsync(refreshToken);

        if (storedRefreshToken is null)
            return RefreshResponse.Failure("RefreshToken does not exist.");

        var jti = validatedToken.Claims.Single(x => x.Type == JwtRegisteredClaimNames.Jti).Value;
        var userIdStr = validatedToken.Claims.Single(x => x.Type == "id").Value;

        if (!long.TryParse(userIdStr, out var userId))
            return RefreshResponse.Failure("UserId in Jwt cannot be parsed.");

        if (storedRefreshToken.JwtId != jti)
            return RefreshResponse.Failure("RefreshToken cannot be used with this JWT token.");

        if (userId != storedRefreshToken.UserId)
            return RefreshResponse.Failure("Users in tokens do not match.");

        if (DateTime.UtcNow > storedRefreshToken.ExpiryTime)
            return RefreshResponse.Failure("RefreshToken has expired.");

        await refreshTokenRepository.DeleteAsync(storedRefreshToken.Id);
        logger.LogRefreshTokenWasRemovedUsed(storedRefreshToken.Token);

        var user = await userRepository.GetByIdAsync(userId);

        if (user is null)
            return RefreshResponse.Failure("User does not exist anymore.");

        var tokens = await GetTokenForUserAsync(user);

        return new RefreshResponse()
        {
            Success = true,
            Token = tokens.Token,
            RefreshToken = tokens.RefreshToken
        };
    }

    public async Task<LogoutResponse> LogoutAsync(string jwt, string refreshToken)
    {
        var validatedToken = GetPrincipalFromToken(jwt);

        if (validatedToken is null)
            return LogoutResponse.Failure("Jwt Token invalid.");

        var storedRefreshToken = await refreshTokenRepository.FindAsync(refreshToken);

        if (storedRefreshToken is null)
            return LogoutResponse.Failure("RefreshToken does not exist.");

        var jti = validatedToken.Claims.Single(x => x.Type == JwtRegisteredClaimNames.Jti).Value;
        var userIdStr = validatedToken.Claims.Single(x => x.Type == "id").Value;

        if (!long.TryParse(userIdStr, out var userId))
            return LogoutResponse.Failure("UserId in Jwt cannot be parsed.");

        if (storedRefreshToken.JwtId != jti)
            return LogoutResponse.Failure("RefreshToken does not match id in JWT token.");

        if (userId != storedRefreshToken.UserId)
            return LogoutResponse.Failure("Users in tokens do not match.");

        await refreshTokenRepository.DeleteAsync(storedRefreshToken.Id);
        logger.LogRefreshTokenWasRemovedLogout(storedRefreshToken.Token);

        return new LogoutResponse() { Success = true };
    }

    private ClaimsPrincipal? GetPrincipalFromToken(string token)
    {
        var tokenHandler = new JwtSecurityTokenHandler();

        try
        {
            var validationParams = tokenValidationParameters.Clone();
            validationParams.ValidateLifetime = false;

            var principal = tokenHandler.ValidateToken(token, validationParams, out var validatedToken);

            return !ValidateJwtAlgorithm(validatedToken) ? null : principal;
        }
        catch
        {
            return null;
        }
    }

    private static bool ValidateJwtAlgorithm(SecurityToken validatedToken)
    {
        return (validatedToken is JwtSecurityToken jwtSecurityToken)
            && jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256,
                StringComparison.InvariantCultureIgnoreCase);
    }



    public async Task<ChangePasswordResponse> ChangePasswordAsync(long id, string password, string newPassword)
    {
        var user = await userRepository.GetByIdAsync(id);

        if (user is null)
            return new ChangePasswordResponse()
            {
                Success = false,
                Errors = new[] { $"User with id {id} does not exist." }
            };

        if(!ValidatePassword(password, user.Password))
            return new ChangePasswordResponse()
            {
                Success = false,
                Errors = new[] { "Password incorrect." }
            };

        await userRepository.ChangePasswordAsync(id, HashPassword(newPassword));

        return new ChangePasswordResponse() {
            Success = true
        };
    }

    public async Task<ChangeUsernameResponse> ChangeUsernameAsync(long id, string password, string newUsername)
    {
        var user = await userRepository.GetByIdAsync(id);

        if (user is null)
            return new ChangeUsernameResponse()
            {
                Success = false,
                Errors = new[] { $"User with id {id} does not exist." }
            };

        if (!ValidatePassword(password, user.Password))
            return new ChangeUsernameResponse()
            {
                Success = false,
                Errors = new[] { $"Password incorrect." }
            };

        await userRepository.ChangeUsernameAsync(id, newUsername);

        return new ChangeUsernameResponse()
        {
            Success = true
        };
    }

    private async Task<(string Token, string RefreshToken)> GetTokenForUserAsync(AppUser user)
    {
        var tokenHandler = new JwtSecurityTokenHandler();
        var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtSettings.Secret));
        var expires = DateTime.UtcNow.Add(jwtSettings.TokenLifetime);
        var tokenDescriptor = new SecurityTokenDescriptor()
        {
            Subject = new ClaimsIdentity(new[] {
                new Claim(JwtRegisteredClaimNames.Sub, user.Username),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim("id", user.Id.ToString()),
                new Claim(ClaimTypes.Role, ((UserRole)user.UserRole).ToString())
            }),
            Expires = expires,
            SigningCredentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256)
        };

        var token = tokenHandler.CreateToken(tokenDescriptor);

        var refreshToken = new byte[256 / 8];
        randomNumberGenerator.GetBytes(refreshToken);

        var newRefreshToken = new RefreshToken()
        {
            CreationTime = DateTime.UtcNow,
            ExpiryTime = DateTime.UtcNow.Add(jwtSettings.RefreshTokenLifetime),
            JwtId = token.Id,
            UserId = user.Id,
            Token = Convert.ToBase64String(refreshToken)
        };
        logger.LogCreatingNewRefreshToken(user.Username);
        await refreshTokenRepository.CreateAsync(newRefreshToken);

        return (tokenHandler.WriteToken(token), newRefreshToken.Token);
    }

    private string HashPassword(string password)
    {
        var salt = new byte[128 / 8];
        randomNumberGenerator.GetBytes(salt);

        var hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: password,
                salt: salt,
                prf: KeyDerivationPrf.HMACSHA1,
                iterationCount: 10000,
                numBytesRequested: 256 / 8
            ));

        return $"{Convert.ToBase64String(salt)}:{hashed}";
    }

    private static bool ValidatePassword(string password, string correctHash)
    {
        var split = correctHash.Split(":");
        var salt = Convert.FromBase64String(split[0]);
        var hash = Convert.FromBase64String(split[1]);

        var hashed = KeyDerivation.Pbkdf2(
                password: password,
                salt: salt,
                prf: KeyDerivationPrf.HMACSHA1,
                iterationCount: 10000,
                numBytesRequested: 256 / 8
            );

        return SlowEquals(hash, hashed);
    }

    private static bool SlowEquals(byte[] a, byte[] b)
    {
        var diff = (uint)a.Length ^ (uint)b.Length;
        for (var i = 0; i < a.Length && i < b.Length; i++)
        {
            diff |= (uint)(a[i] ^ b[i]);
        }
        return diff == 0;
    }
}

internal static partial class AuthServiceLogExtensions
{
    [LoggerMessage(
        Level = LogLevel.Information,
        Message = "Creating new RefreshToken for User '{Username}'")]
    internal static partial void LogCreatingNewRefreshToken(this ILogger logger, string username);

    [LoggerMessage(
        Level = LogLevel.Information,
        Message = "RefreshToken {Token} was removed (logout)")]
    internal static partial void LogRefreshTokenWasRemovedLogout(this ILogger logger, string token);

    [LoggerMessage(
        Level = LogLevel.Information,
        Message = "RefreshToken {Token} was removed (used)")]
    internal static partial void LogRefreshTokenWasRemovedUsed(this ILogger logger, string token);
}