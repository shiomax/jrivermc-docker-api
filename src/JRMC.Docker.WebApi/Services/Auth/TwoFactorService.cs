using System.Security.Cryptography;
using JRMC.Docker.DataAccess.Contracts;
using JRMC.Docker.DataAccess.Entities;
using JRMC.Docker.WebApi.Services.Auth.Contracts;
using OtpNet;

namespace JRMC.Docker.WebApi.Services.Auth;

public class TwoFactorService(IUserRepository userRepository,
        ISystemClock systemClock,
        RandomNumberGenerator randomNumberGenerator)
    : ITwoFactorService
{
    public async Task<bool> ValidateTwoFactorAsync(AppUser user, string code)
    {
        if (string.IsNullOrEmpty(code)) return false;
        
        var twoFactors = await userRepository.GetTwoFactorMethodsForUserAsync(user.Id);

        foreach (var twoFactor in twoFactors)
        {
            switch (twoFactor)
            {
                case { TwoFactorMethod: (int) TwoFactorMethod.Totp }:
                    var totp = new Totp(twoFactor.Secret).ComputeTotp(systemClock.UtcNow);
                    if (totp == code) return true;
                    break;
                
                default:
                    throw new NotSupportedException($"Two Factor method {twoFactor.TwoFactorMethod} not supported");
            }
        }

        return false;
    }

    public byte[] GenerateSecret()
    {
        var secret = new byte[20];
        randomNumberGenerator.GetBytes(secret);
        return secret;
    }
}