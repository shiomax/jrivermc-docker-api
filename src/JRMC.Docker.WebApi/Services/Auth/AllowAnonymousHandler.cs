using Microsoft.AspNetCore.Authorization;

namespace JRMC.Docker.WebApi.Services.Auth;

/// <summary>
/// Authorization Handler that accepts anything (disable authorization)
/// </summary>
public class AllowAnonymousHandler : IAuthorizationHandler
{
    public Task HandleAsync(AuthorizationHandlerContext context)
    {
        foreach (var requirement in context.PendingRequirements.ToList())
            context.Succeed(requirement);
        
        return Task.CompletedTask;
    }
}