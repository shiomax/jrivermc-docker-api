﻿using JRMC.Docker.DataAccess.Contracts;

namespace JRMC.Docker.WebApi.Services;

public class CleanupService(
        IRefreshTokenRepository refreshTokenRepository,
        CleanupServiceSettings cleanupServiceSettings,
        ILogger<CleanupService> logger,
        ISystemClock systemClock)
    : BackgroundService
{
    private readonly PeriodicTimer timer =  new(TimeSpan.FromHours(1));

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        // Disabled when Authentication is disabled
        if (!cleanupServiceSettings.Enabled) return;
        
        if(cleanupServiceSettings.ShouldClearTokensOnStart) 
        {
            var deletedCount = await refreshTokenRepository.ClearAllAsync();
            logger.LogDeletedRefreshTokensOnStart(deletedCount);
            cleanupServiceSettings.ShouldClearTokensOnStart = false;
        }
        else
        {
            await CleanupExpiredRefreshTokens();
        }

        while(!stoppingToken.IsCancellationRequested) 
        {
            await timer.WaitForNextTickAsync(stoppingToken);

            await CleanupExpiredRefreshTokens();
        }
    }

    private async Task CleanupExpiredRefreshTokens()
    {
        var deletedCount = await refreshTokenRepository.DeleteAllExpiredAsync(systemClock.UtcNow);

        if (deletedCount > 0)
            logger.LogDeletedRefreshTokens(deletedCount);
    }
}

internal static partial class CleanupServiceLogExtensions
{
    [LoggerMessage(
        Level = LogLevel.Information,
        Message = "Deleted {DeletedCount} expired RefreshTokens")]
    internal static partial void LogDeletedRefreshTokens(this ILogger logger, int deletedCount);

    [LoggerMessage(
        Level = LogLevel.Information,
        Message = "Deleted {DeletedCount} RefreshTokens (JWT secret changed)")]
    internal static partial void LogDeletedRefreshTokensOnStart(this ILogger logger, int deletedCount);
}

public class CleanupServiceSettings 
{
    public required bool Enabled { get; init; }
    
    /* True when JWT secret was re-create on startup */
    public bool ShouldClearTokensOnStart { get; set; }
}
