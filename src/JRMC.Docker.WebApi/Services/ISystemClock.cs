namespace JRMC.Docker.WebApi.Services;

public interface ISystemClock
{
    public DateTime UtcNow { get; }
}