﻿using JRMC.Docker.WebApi.Contracts.Responses.Activation;
using System.Diagnostics;
using JRMC.Docker.WebApi.Services.Activation.Contracts;

namespace JRMC.Docker.WebApi.Services.Activation;

public class ActivationService : IActivationService
{
    private const string MjrFilePath = "/config/appstorage/key.mjr";
    private Task? task;
    private readonly SemaphoreSlim semaphore = new(1, 1);

    private bool IsRunning => task?.Status is TaskStatus.WaitingForActivation or TaskStatus.Running;

    public ActivationStatusResponse Status { get; private set; } = new();

    public async Task<PostActivateMjrFileResponse> ActivateJRiverAsync(IFormFile file)
    {
        if (IsRunning) return new PostActivateMjrFileResponse()
        {
            Success = false,
            Errors = new[] { "Activation in progress." }
        };

        try
        {
            await semaphore.WaitAsync();
            if (IsRunning) return new PostActivateMjrFileResponse()
            {
                Success = false,
                Errors = new[] { "Activation in progress." }
            };

            await StartActivationTask(file);

            return new PostActivateMjrFileResponse()
            {
                Success = true
            };
        } finally
        {
            semaphore.Release();
        }
    }

    private async Task StartActivationTask(IFormFile file)
    {
        if (File.Exists(MjrFilePath)) File.Delete(MjrFilePath);

        await using (var stream = new FileStream(MjrFilePath, FileMode.Create))
            await file.CopyToAsync(stream);
        
        Status = new ActivationStatusResponse()
        {
            IsRunning = true
        };

        task = Task.Run(async () => {
            try {
                await ActivationTask();
            } catch(Exception e) {
                Status.IsRunning = false;
                Status.Errors = new[] { e.Message };
                Status.ExitCode = -1;
                Status.Success = false;
            } 
        });
    }

    private async Task ActivationTask()
    {
        using var process = new Process();
        process.StartInfo.WorkingDirectory = "/tmp";
        process.StartInfo.FileName = "mcd-activate";
        process.StartInfo.RedirectStandardError = false;
        process.StartInfo.RedirectStandardOutput = true;
        process.StartInfo.Arguments = MjrFilePath;
        process.StartInfo.UseShellExecute = false;
        process.Start();

        var processOutput = new List<string>();
        Status.Output = processOutput;
        var stream = process.StandardOutput;

        string? line;
        while((line = await stream.ReadLineAsync()) is not null)
        {
            processOutput.Add(line);
        }

        await process.WaitForExitAsync();

        Status.ExitCode = process.ExitCode;
        Status.IsRunning = false;

        if (Status.ExitCode < 0)
        {
            Status.Success = false;
            
            Status.Errors = new[] { $"Exit code '{Status.ExitCode}' does not indicate success." };
        }
        else
        {
            Status.Success = true;
        }
    }
}
