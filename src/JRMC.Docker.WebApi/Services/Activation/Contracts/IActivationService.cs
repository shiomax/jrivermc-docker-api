﻿using JRMC.Docker.WebApi.Contracts.Responses.Activation;

namespace JRMC.Docker.WebApi.Services.Activation.Contracts;

public interface IActivationService
{
    Task<PostActivateMjrFileResponse> ActivateJRiverAsync(IFormFile file);

    ActivationStatusResponse Status { get; }
}
