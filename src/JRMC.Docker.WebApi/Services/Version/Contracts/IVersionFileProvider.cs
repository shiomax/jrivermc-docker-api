namespace JRMC.Docker.WebApi.Services.Version.Contracts;

public interface IVersionFileProvider
{
    Stream OpenRead();
    
    bool Exists { get; }
}