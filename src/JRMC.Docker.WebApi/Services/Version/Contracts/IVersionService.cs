using JRMC.Docker.WebApi.Contracts.Responses.General;

namespace JRMC.Docker.WebApi.Services.Version.Contracts;

public interface IVersionService
{
    ValueTask<GetVersionResponse> GetVersionResponseAsync();
}