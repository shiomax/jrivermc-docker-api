using JRMC.Docker.WebApi.Services.Version.Contracts;
using JRMC.Docker.WebApi.Settings;

namespace JRMC.Docker.WebApi.Services.Version;

public class VersionFileProvider(GeneralSettings generalSettings) : IVersionFileProvider
{
    public Stream OpenRead() => File.OpenRead(generalSettings.VersionFile!);

    public bool Exists => !string.IsNullOrWhiteSpace(generalSettings?.VersionFile) && File.Exists(generalSettings.VersionFile);
}