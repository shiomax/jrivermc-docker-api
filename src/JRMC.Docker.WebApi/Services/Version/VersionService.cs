using JRMC.Docker.WebApi.Contracts.Responses.General;
using JRMC.Docker.WebApi.Services.Version.Contracts;

namespace JRMC.Docker.WebApi.Services.Version;

public class VersionService(IVersionFileProvider versionFileProvider) : IVersionService
{
    private GetVersionResponse? getVersionResponse;
    private readonly SemaphoreSlim getVersionResponseLock = new(1, 1);

    public async ValueTask<GetVersionResponse> GetVersionResponseAsync()
    {
        if (getVersionResponse is not null) return getVersionResponse;

        await getVersionResponseLock.WaitAsync();
        
        var imageVersion = "unknown";
        var debUrl = "unknown";
        var usesRepository = true;
        var mediaCenterTag = "unknown";
        
        try
        {
            if (versionFileProvider.Exists)
            {
                await using var fs = versionFileProvider.OpenRead();
                using var sr = new StreamReader(fs);

                while (await sr.ReadLineAsync() is { } line)
                {
                    if (string.IsNullOrWhiteSpace(line)) continue;

                    var split = line.Trim().Split(" ");
                    switch (split)
                    {
                        case ["DEB_URL", var debUrlValue, ..]:
                            debUrl = debUrlValue;
                            usesRepository = debUrlValue == "repository";
                            break;
                        case ["IMAGE_VERSION", var imgVersionValue, ..]:
                            imageVersion = imgVersionValue;
                            break;
                        case ["JRIVER_TAG", var mediaCenterTagValue, ..]:
                            mediaCenterTag = mediaCenterTagValue;
                            break;
                    }
                }
            }

            getVersionResponse = new GetVersionResponse()
            {
                DebUrl = debUrl,
                ImageVersion = imageVersion,
                UsesRepository = usesRepository,
                MediaCenterTag = mediaCenterTag
            };
        }
        finally
        {
            getVersionResponseLock.Release();
        }

        return getVersionResponse;
    }
}