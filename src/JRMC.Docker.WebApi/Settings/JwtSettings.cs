﻿namespace JRMC.Docker.WebApi.Settings;

public class JwtSettings
{
    public bool Enabled { get; init; }
    
    public bool GenerateSecret { get; init; }

    public bool StoreSecret { get; init; }

    public string Secret { get; set; } = default!;

    public string SecretStore { get; init; } = default!;

    public TimeSpan TokenLifetime { get; set; }

    public TimeSpan RefreshTokenLifetime { get; set; }

    public static JwtSettings Get(IConfiguration configuration)
    {
        var section = configuration.GetRequiredSection(nameof(JwtSettings));

        return new JwtSettings()
        {
            Enabled = section[nameof(Enabled)]?.ToLower() is "true" or "1",
            GenerateSecret = section[nameof(GenerateSecret)]?.ToLower() is "true" or "1",
            SecretStore = section[nameof(SecretStore)] ?? string.Empty,
            StoreSecret = section[nameof(StoreSecret)]?.ToLower() is "true" or "1",
            TokenLifetime = TimeSpan.Parse(section[nameof(TokenLifetime)] ?? "00:05:00"),
            RefreshTokenLifetime = TimeSpan.Parse(section[nameof(RefreshTokenLifetime)] ?? "2.00:00:00"),
            Secret = section[nameof(Secret)] ?? string.Empty
        };
    }
}
