namespace JRMC.Docker.WebApi.Settings;

public class TwoFactorSettings
{
    public bool Enabled { get; init; }

    public static TwoFactorSettings Get()
    {
        var enabled = Environment.GetEnvironmentVariable("DISABLE_2FA") != "1";
        
        return new TwoFactorSettings()
        {
            Enabled = enabled
        };
    }
}