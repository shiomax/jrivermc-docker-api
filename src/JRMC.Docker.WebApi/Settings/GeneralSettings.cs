namespace JRMC.Docker.WebApi.Settings;

public class GeneralSettings
{
    public string? VersionFile { get; init; }

    public static GeneralSettings Get(IConfiguration configuration)
    {
        var section = configuration.GetRequiredSection(nameof(GeneralSettings));
        
        return new GeneralSettings()
        {
            VersionFile = section[nameof(VersionFile)]
        };
    }
}