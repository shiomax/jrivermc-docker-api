﻿using JRMC.Docker.DataAccess.Contracts;

namespace JRMC.Docker.WebApi.Settings;

public class DatabaseSettings : IDatabaseSettings
{
    public string ConnectionString { get; init; } = string.Empty;

    public static DatabaseSettings Get(IConfiguration configuration)
    {
        var section = configuration.GetRequiredSection(nameof(DatabaseSettings));

        return new DatabaseSettings()
        {
            ConnectionString = section[nameof(ConnectionString)] ?? string.Empty
        };
    }
}
