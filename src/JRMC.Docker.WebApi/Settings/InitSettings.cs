namespace JRMC.Docker.WebApi.Settings;

public  class InitSettings 
{
    public int InitialHeight { get; init; }

    public int InitialWidth { get; init; }

    public static InitSettings Get()
    {
        const int defaultHeight = 768;
        const int defaultWidth = 1280;

        const string keyWidth = "DISPLAY_WIDTH";
        const string keyHeight = "DISPLAY_HEIGHT";

        var widthStr = Environment.GetEnvironmentVariable(keyWidth);
        var heightStr = Environment.GetEnvironmentVariable(keyHeight);

        if(int.TryParse(widthStr, out var width) && int.TryParse(heightStr, out var height)) 
        {
            return new InitSettings() 
            {
                InitialHeight = height,
                InitialWidth = width
            };
        }

        return new InitSettings() 
        {
            InitialHeight = defaultHeight,
            InitialWidth = defaultWidth
        };
    }
}