using System.Runtime.InteropServices;
using JRMC.Docker.DataAccess.Migrations;
using JRMC.Docker.WebApi.Endpoints;
using JRMC.Docker.WebApi.Extensions.Setup;
using JRMC.Docker.WebApi.Generators;
using JRMC.Docker.WebApi.Utils;
using Microsoft.AspNetCore.HttpOverrides;

var builder = WebApplication.CreateSlimBuilder(args);
builder.Logging.ClearProviders();
builder.Logging.AddConsole();

// Logger for Startup
var logger = LoggerFactory.Create(config =>
{
    config.AddConsole();
}).CreateLogger("Program");

// Disable Auth when environment variable is set to 1
if (Environment.GetEnvironmentVariable("DISABLE_AUTH") == "1")
{
    logger.LogInformation("Authentication is disabled");
    builder.Configuration.AddInMemoryCollection(new KeyValuePair<string, string?>[]
    {
        new("JwtSettings:Enabled", "false")
    });
}

// Add services to the container.
builder.Services.AddAuthServices(builder.Configuration, logger);
builder.Services.AddBaseServices(builder.Configuration);
builder.Services.AddAppServices(builder.Configuration);

// Bind to unix socket if configured
var unixSocket = builder.Configuration["ServerSettings:UnixSocket"];
if(!string.IsNullOrEmpty(unixSocket)) {
    builder.WebHost.ConfigureKestrel(options => {
        options.ListenUnixSocket(unixSocket);
    });
}

var app = builder.Build();

// Configure the HTTP request Pipeline
if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
}

app.UseRouting(); // Use Endpoint Routing

app.UseCors("CorsPolicy"); // CORS Policy defined in Services

// For NGINX Reverse Proxy
app.UseForwardedHeaders(new ForwardedHeadersOptions
{
    ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
});

app.UseAuthentication();
app.UseAuthorization();

// Route Setup
app.RegisterEndpoints();

// Run Migrations
Migrations.Apply(builder.Configuration["DatabaseSettings:ConnectionString"]);

await app.StartAsync();

// change permission of configured unix socket
if(!string.IsNullOrEmpty(unixSocket))
{
    const UnixFilePermission _0666 = UnixFilePermission.UserRead
                                     | UnixFilePermission.UserWrite
                                     | UnixFilePermission.GroupRead
                                     | UnixFilePermission.GroupWrite
                                     | UnixFilePermission.OthersRead
                                     | UnixFilePermission.OthersWrite;

    var exitCode = Chmod.Invoke(unixSocket, _0666);
    if (exitCode != 0)
        app.Logger.LogError("Failed to set permission on socket ({ExitCode})", Marshal.GetLastPInvokeError());
}

await app.WaitForShutdownAsync();
