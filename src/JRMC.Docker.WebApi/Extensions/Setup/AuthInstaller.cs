﻿using System.Security.Cryptography;
using System.Text;
using JRMC.Docker.WebApi.Services;
using JRMC.Docker.WebApi.Services.Auth;
using JRMC.Docker.WebApi.Services.Auth.Contracts;
using JRMC.Docker.WebApi.Settings;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.IdentityModel.Tokens;

namespace JRMC.Docker.WebApi.Extensions.Setup;

public static class AuthInstaller
{
    public static void AddAuthServices(this IServiceCollection services, IConfiguration configuration, ILogger logger)
    {
        var jwtSettings = GetJwtSettings(configuration, logger);
        services.AddSingleton(jwtSettings);

        var rng = RandomNumberGenerator.Create();
        services.AddSingleton(rng);

        var cleanupServiceSettings = new CleanupServiceSettings()
        {
            Enabled = jwtSettings.Enabled
        };
        GenerateSecret(rng, jwtSettings, cleanupServiceSettings, logger);
        services.AddSingleton(cleanupServiceSettings);

        var tokenValidationParameters = new TokenValidationParameters
        {
            ValidateIssuerSigningKey = true,
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtSettings.Secret)),
            ValidateIssuer = false,
            ValidateAudience = false,
            RequireExpirationTime = true,
            ValidateLifetime = true
        };

        services.AddSingleton(tokenValidationParameters);
        services.AddSingleton<IAuthService, AuthService>();
        
        services.AddAuthentication(options =>
        {
            options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
        }).AddJwtBearer(options =>
        {
            options.SaveToken = true;
            options.TokenValidationParameters = tokenValidationParameters;
        });

        services.AddAuthorization(options =>
        {
            options.FallbackPolicy = new AuthorizationPolicyBuilder()
                .AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme)
                .RequireAuthenticatedUser()
                .Build();
        });
        
        // Add handler that accepts anything when authorization is diabled
        if(!jwtSettings.Enabled)
            services.AddSingleton<IAuthorizationHandler, AllowAnonymousHandler>();
    }

    private static JwtSettings GetJwtSettings(IConfiguration configuration, ILogger logger)
    {
        var jwtSettings = JwtSettings.Get(configuration);
        if (jwtSettings is null) throw new NullReferenceException($"{nameof(jwtSettings)} could not be read");

        var refreshTokenLifetime = Environment.GetEnvironmentVariable("REFRESH_TOKEN_LIFETIME");

        if (!string.IsNullOrEmpty(refreshTokenLifetime))
        {
            if (TimeSpan.TryParse(refreshTokenLifetime, out var parsedTimespan))
            {
                jwtSettings.RefreshTokenLifetime = parsedTimespan;
            }
            else logger.LogError("{RefreshTokenLifetime} is not a valid Timespan", refreshTokenLifetime);
        }

        var jwtTokenLifetime = Environment.GetEnvironmentVariable("JWT_TOKEN_LIFETIME");

        if (!string.IsNullOrEmpty(jwtTokenLifetime))
        {
            if (TimeSpan.TryParse(jwtTokenLifetime, out var parsedTimespan))
            {
                jwtSettings.TokenLifetime = parsedTimespan;
            }
            else logger.LogError("{JwtTokenLifetime} is not a valid Timespan", jwtTokenLifetime);
        }

        return jwtSettings;
    }

    private static void GenerateSecret(
        RandomNumberGenerator rng, 
        JwtSettings jwtSettings,
        CleanupServiceSettings cleanupServiceSettings,
        ILogger logger)
    {
        if (!jwtSettings.GenerateSecret) return;

        // Using Encoding without BOM, so python does not read 'zero width no break space'
        var encoding = new UTF8Encoding(encoderShouldEmitUTF8Identifier: false);
        if (jwtSettings.StoreSecret && File.Exists(jwtSettings.SecretStore))
        {
            jwtSettings.Secret = File.ReadAllText(jwtSettings.SecretStore, encoding).Trim();
            return;
        }

        // Indicates that all refresh tokens should be deleted on startup
        cleanupServiceSettings.ShouldClearTokensOnStart = true;
        logger.LogInformation("Generating new jwt secret (existing logins will expire)");

        var randomBytes = new byte[32];
        rng.GetBytes(randomBytes);
        jwtSettings.Secret = Convert.ToBase64String(randomBytes);
        if(jwtSettings is { StoreSecret: true, Enabled: true })
            File.WriteAllText(jwtSettings.SecretStore, jwtSettings.Secret, encoding);
    }
}
