﻿using JRMC.Docker.WebApi.Contracts.Serialization;
using Microsoft.AspNetCore.Http.Json;

namespace JRMC.Docker.WebApi.Extensions.Setup;

public static class BaseInstaller
{
    public static void AddBaseServices(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddCors(options =>
        {
            options.AddPolicy("CorsPolicy", builder =>
            {
                builder.AllowAnyMethod();
                builder.AllowAnyHeader();
                builder.AllowCredentials();
                builder.SetIsOriginAllowed(_ => true);
            });
        });
        
        services.Configure<JsonOptions>(options =>
        {
            options.SerializerOptions.TypeInfoResolverChain.Insert(0, ApiJsonSerializerContext.Default);
        });

        services.AddLogging(builder =>
        {
            builder.ClearProviders();
            builder.AddConsole();
        });
    }
}
