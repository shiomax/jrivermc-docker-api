﻿using JRMC.Docker.DataAccess;
using JRMC.Docker.DataAccess.Contracts;
using JRMC.Docker.WebApi.Services;
using JRMC.Docker.WebApi.Services.Activation;
using JRMC.Docker.WebApi.Services.Activation.Contracts;
using JRMC.Docker.WebApi.Services.Auth;
using JRMC.Docker.WebApi.Services.Auth.Contracts;
using JRMC.Docker.WebApi.Services.Version;
using JRMC.Docker.WebApi.Services.Version.Contracts;
using JRMC.Docker.WebApi.Settings;

namespace JRMC.Docker.WebApi.Extensions.Setup;

public static class ServiceInstaller
{
    public static void AddAppServices(this IServiceCollection services, IConfiguration configuration)
    {
        var databaseSettings = DatabaseSettings.Get(configuration);
        services.AddSingleton<IDatabaseSettings>(databaseSettings);

        services.AddSingleton<GeneralSettings>(
            serviceProvider => GeneralSettings.Get(serviceProvider.GetRequiredService<IConfiguration>()));

        services.AddSingleton(InitSettings.Get());
        
        services.AddSqliteServices(databaseSettings);

        services.AddSingleton<IActivationService, ActivationService>();
        services.AddSingleton<IVersionFileProvider, VersionFileProvider>();
        services.AddSingleton<IVersionService, VersionService>();
        services.AddHostedService<CleanupService>();
        services.AddSingleton<ISystemClock, SystemClock>();
        services.AddSingleton<ITwoFactorService, TwoFactorService>();

        services.AddSingleton(TwoFactorSettings.Get());
    }
}
