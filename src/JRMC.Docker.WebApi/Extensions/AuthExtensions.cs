﻿using JRMC.Docker.DataAccess.Entities;
using System.Security.Claims;

namespace JRMC.Docker.WebApi.Extensions;

public static class AuthExtensions
{
    public static long GetUserId(this HttpContext httpContext)
    {
        if (httpContext.User is null)
            throw new NullReferenceException("User is not set");

        var strValue = httpContext.User.Claims.Single(x => x.Type == "id").Value;

        if (long.TryParse(strValue, out var result))
            return result;

        throw new ArgumentNullException($"UserId '{strValue}' is not a number");
    }

    public static UserRole GetUserRole(this HttpContext httpContext)
    {
        if (httpContext.User is null)
            throw new NullReferenceException("User is not set");

        var strValue = httpContext.User.Claims.Single(x => x.Type == ClaimTypes.Role).Value;

        if (Enum.TryParse(typeof(UserRole), strValue, out var result))
            return (UserRole) result;

        throw new ArgumentNullException($"UserRole '{strValue}' is invalid");
    }
}
