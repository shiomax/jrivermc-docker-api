#!/bin/bash

find ./tests -maxdepth 1 -mindepth 1 -type d | while read d; do
    dotnet test $d/
done
