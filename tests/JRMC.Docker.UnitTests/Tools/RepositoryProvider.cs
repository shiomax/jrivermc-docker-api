﻿using JRMC.Docker.DataAccess.Contracts;
using System;
using System.IO;
using JRMC.Docker.DataAccess;
using JRMC.Docker.DataAccess.Migrations;
using Microsoft.Data.Sqlite;
using Microsoft.Extensions.DependencyInjection;

namespace JRMC.Docker.UnitTests.Tools;

internal class RepositoryProvider : IDisposable
{
    public IUserRepository UserRepository => services.GetRequiredService<IUserRepository>();
    public IRefreshTokenRepository RefreshTokenRepository => services.GetRequiredService<IRefreshTokenRepository>();
    public IDatabaseSettings DatabaseSettings { get; }

    private readonly string guid;
    private readonly IServiceProvider services;
    
    public RepositoryProvider(string? existingDatabase = null)
    {
        guid = Guid.NewGuid().ToString();
        
        // Copy existing db
        if (!string.IsNullOrEmpty(existingDatabase))
            File.Copy(sourceFileName: $"Data/{existingDatabase}", destFileName: $"{guid}.sqlite");

        DatabaseSettings = new DatabaseSettings()
        {
            ConnectionString = $"Data Source={guid}.sqlite;Pooling=False;"
        };
        
        Migrations.Apply(DatabaseSettings.ConnectionString);
        services = BuildServiceProvider(DatabaseSettings);
    }

    private static IServiceProvider BuildServiceProvider(IDatabaseSettings settings)
    {
        var builder = new ServiceCollection();
        builder.AddSingleton(settings);
        builder.AddSqliteServices(settings);
        return builder.BuildServiceProvider();
    }

    public void Dispose()
    {
        SqliteConnection.ClearAllPools();
        File.Delete($"{guid}.sqlite");
    }
}
