﻿using JRMC.Docker.DataAccess.Contracts;

namespace JRMC.Docker.UnitTests.Tools;

internal class DatabaseSettings : IDatabaseSettings
{
    public string ConnectionString { get; internal init; } = string.Empty;
}
