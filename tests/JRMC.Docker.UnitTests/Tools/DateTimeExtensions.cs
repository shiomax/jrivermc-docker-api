﻿using System;

namespace JRMC.Docker.UnitTests.Tools;

internal static class DateTimeExtensions
{
    internal static DateTime RemoveMilliseconds(this DateTime dateTime)
    {
        return new DateTime(dateTime.Year, dateTime.Month, dateTime.Day,
            dateTime.Hour, dateTime.Minute, dateTime.Second, dateTime.Kind);
    }
}
