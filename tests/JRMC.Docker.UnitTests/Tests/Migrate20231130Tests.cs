using System;
using System.Linq;
using Dapper;
using JRMC.Docker.DataAccess.Entities;
using Microsoft.Data.Sqlite;

namespace JRMC.Docker.UnitTests.Tests;

public class Migrate20231130Tests : IDisposable
{
    private readonly RepositoryProvider repositoryProvider = new("2023_11_30_EFCore.sqlite");

    [Fact]
    public async Task User_Should_Still_Exist()
    {
        // Act
        var user = await repositoryProvider.UserRepository.FindAsync("max");
        var usersCount = await repositoryProvider.UserRepository.CountAsync();
        
        // Assert
        user.Should().NotBeNull();
        usersCount.Should().Be(1);
        user?.Password.Should().Be("<password>");
    }

    [Fact]
    public async Task Refresh_Token_Repo_Should_Work()
    {
        // Arrange
        var refreshToken = new RefreshToken()
        {
            Token = "Some Token",
            ExpiryTime = new DateTime(2022, 10, 10, 0, 0, 0, DateTimeKind.Utc),
            CreationTime = new DateTime(2022, 10, 10, 0, 0, 0, DateTimeKind.Utc),
            JwtId = "Some Id",
            UserId = 1
        };

        // Act
        int countBefore;
        await using (var connection = new SqliteConnection(repositoryProvider.DatabaseSettings.ConnectionString))
        {
#pragma warning disable DAP005
            countBefore = await connection.ExecuteScalarAsync<int>("SELECT count(*) FROM RefreshToken");
#pragma warning restore DAP005
        }
        
        refreshToken.Id = await repositoryProvider.RefreshTokenRepository.CreateAsync(refreshToken);
        var insertedToken = await repositoryProvider.RefreshTokenRepository.FindAsync("Some Token");

        // Assert
        insertedToken.Should().BeEquivalentTo(refreshToken);
        countBefore.Should().Be(0);
    }

    [Fact]
    public async Task TwoFactor_ShouldStill_Exist()
    {
        // Act
        var user = await repositoryProvider.UserRepository.FindAsync("max");
        var twoFactors = await repositoryProvider.UserRepository.GetTwoFactorMethodsForUserAsync(user!.Id);

        // Assert
        user.EnableTwoFa.Should().BeTrue();
        twoFactors.Should().HaveCount(1);
        twoFactors.First().Name.Should().Be("Phone");
        twoFactors.First().TwoFactorMethod.Should().Be((int)TwoFactorMethod.Totp);
    }

    public void Dispose()
    {
        repositoryProvider.Dispose();
    }
}