using System.Text;
using JRMC.Docker.WebApi.Utils;

namespace JRMC.Docker.UnitTests.Tests;

public class Base32Tests
{
    [Theory]
    [InlineData("Hello World", "JBSWY3DPEBLW64TMMQ======")]
    [InlineData("The quick brown fox jumps over the lazy dog.", "KRUGKIDROVUWG2ZAMJZG653OEBTG66BANJ2W24DTEBXXMZLSEB2GQZJANRQXU6JAMRXWOLQ=")]
    public void Base32Encoder_ShouldEncodeCorrectly(string inputText, string expected)
    {
        // Arrange
        var input = Encoding.UTF8.GetBytes(inputText);
        
        // Act
        var result = Base32Encoder.Encode(input);
        
        // Assert
        result.Should().Be(expected);
    }
}