﻿using System;
using System.Linq;
using JRMC.Docker.DataAccess.Entities;
using Microsoft.Data.Sqlite;

namespace JRMC.Docker.UnitTests.Tests;

public class UserRepositoryTests : IDisposable
{
    private readonly RepositoryProvider repositoryProvider = new();

    [Fact]
    public async Task IsEmpty_Should_ReturnTrue_OnEmptyDb()
    {
        // Arrange
        var repo = repositoryProvider.UserRepository;

        // Act
        var isEmpty = await repo.IsEmptyAsync();

        // Assert
        isEmpty.Should().BeTrue();
    }

    [Fact]
    public async Task CreateUser_Should_CreateUser()
    {
        // Arrange
        var repo = repositoryProvider.UserRepository;

        // Act
        var id = await repo.CreateAsync(new AppUser()
        {
            Username = "user", 
            Password = "passwordHash"
        });
        var isEmpty = await repo.IsEmptyAsync();
        var findUser = await repo.FindAsync("user", "passwordHash");

        // Assert
        isEmpty.Should().BeFalse();
        id.Should().BeGreaterThan(0);
        findUser.Should().BeEquivalentTo(new AppUser() { 
            Id = id,
            Username = "user",
            Password = "passwordHash"
        });
    }

    [Fact]
    public async Task ChangePassword_Should_ChangePassword()
    {
        // Arrange
        var repo = repositoryProvider.UserRepository;

        // Act
        var id = await repo.CreateAsync(new AppUser()
        {
            Username = "user",
            Password = "passwordHash"
        });
        await repo.ChangePasswordAsync(id, "newPassword");
        var findUser = await repo.FindAsync("user", "newPassword");
        
        // Assert
        findUser.Should().BeEquivalentTo(new AppUser()
        {
            Id = id,
            Username = "user",
            Password = "newPassword"
        });
    }

    [Fact]
    public async Task ChangeUsername_Should_ChangeUsername()
    {
        // Arrange
        var repo = repositoryProvider.UserRepository;

        // Act
        var id = await repo.CreateAsync(new AppUser()
        {
            Username = "user",
            Password = "passwordHash"
        });
        await repo.ChangeUsernameAsync(id, "newUsername");
        var findUser = await repo.FindAsync("newUsername", "passwordHash");

        // Assert
        findUser.Should().BeEquivalentTo(new AppUser()
        {
            Id = id,
            Username = "newUsername",
            Password = "passwordHash"
        });
    }

    [Fact]
    public async Task Multiple_Inserts_Should_Increment_Id()
    {
        // Arrange
        var repo = repositoryProvider.UserRepository;

        // Act
        var id = await repo.CreateAsync(new AppUser()
        {
            Username = "user1",
            Password = "passwordHash"
        });
        var id2 = await repo.CreateAsync(new AppUser()
        {
            Username = "user2",
            Password = "passwordHash"
        });

        // Assert
        id.Should().NotBe(id2);
    }

    [Fact]
    public async Task FindUser_ShouldReturnNull_WhenPasswordDoesNotMatch()
    {
        // Arrange
        var repo = repositoryProvider.UserRepository;

        // Act
        await repo.CreateAsync(new AppUser()
        {
            Username = "user",
            Password = "passwordHash"
        });
        var findUser = await repo.FindAsync("user", "wrongPassword");

        // Assert
        findUser.Should().BeNull();
    }

    [Fact]
    public async Task FindUser_ShouldReturnUser_WithoutPassword()
    {
        // Arrange
        var repo = repositoryProvider.UserRepository;

        // Act
        var id = await repo.CreateAsync(new AppUser()
        {
            Username = "user",
            Password = "passwordHash"
        });
        var findUser = await repo.FindAsync("user");

        // Assert
        findUser.Should().NotBeNull();
        findUser.Should().BeEquivalentTo(new AppUser() { 
            Id = id,
            Username = "user",
            Password = "passwordHash"
        });
    }

    [Fact]
    public async Task GetById_Should_Return_User()
    {
        // Arrange
        var repo = repositoryProvider.UserRepository;

        // Act
        var id = await repo.CreateAsync(new AppUser()
        {
            Username = "user",
            Password = "passwordHash"
        });
        var getByIdResult = await repo.GetByIdAsync(id);

        // Assert
        getByIdResult.Should().BeEquivalentTo(new AppUser()
        {
            Id = id,
            Username = "user",
            Password = "passwordHash"
        });
    }

    [Fact]
    public async Task UserRole_Should_PersistProperly()
    {
        // Arrange
        var repo = repositoryProvider.UserRepository;
        var member = new AppUser() 
        { 
            Username = "Member",
            Password = "password",
            UserRole = (int) UserRole.Member
        };
        var owner = new AppUser()
        {
            Username = "Owner",
            Password = "password",
            UserRole = (int) UserRole.Owner
        };
        var admin = new AppUser()
        {
            Username = "Admin",
            Password = "password",
            UserRole = (int) UserRole.Admin
        };

        // Act
        await repo.CreateAsync(member);
        await repo.CreateAsync(owner);
        await repo.CreateAsync(admin);
        var storedAdmin = await repo.FindAsync("Admin");
        var storedOwner = await repo.FindAsync("Owner");
        var storedMember = await repo.FindAsync("Member");

        // Assert
        storedAdmin!.UserRole.Should().Be((int) UserRole.Admin);
        storedOwner!.UserRole.Should().Be((int) UserRole.Owner);
        storedMember!.UserRole.Should().Be((int) UserRole.Member);
    }

    [Fact]
    public async Task CreateToFactor_Should_CreateTwoFactor()
    {
        // Arrange
        var timeOfTest = DateTime.UtcNow;
        var repo = repositoryProvider.UserRepository;
        var member = new AppUser() 
        { 
            Username = "Member",
            Password = "password",
            UserRole = (int) UserRole.Member
        };
        var twoFactor = new TwoFactor()
        {
            Name = "Test",
            Secret = "Something"u8.ToArray(),
            TwoFactorMethod = (int) TwoFactorMethod.Totp
        };
        
        // Act
        var id = await repo.CreateAsync(member);
        twoFactor.UserId = id;
        var twoFaId = await repo.CreateTwoFactorMethodAsync(twoFactor);
        var getAllForUser = await repo.GetTwoFactorMethodsForUserAsync(id);
        
        // Assert
        getAllForUser.Should().HaveCount(1);
        getAllForUser.First().Name.Should().Be(twoFactor.Name);
        getAllForUser.First().Secret.Should().BeEquivalentTo(twoFactor.Secret);
        getAllForUser.First().TwoFactorMethod.Should().Be((int) TwoFactorMethod.Totp);
        getAllForUser.First().CreatedAt.Should().BeOnOrAfter(timeOfTest);
        getAllForUser.First().Id.Should().Be(twoFaId);
        getAllForUser.First().UserId.Should().Be(id);
    }

    [Fact]
    public async Task DeletingUser_Should_Delete2fa()
    {
        // Arrange
        var repo = repositoryProvider.UserRepository;
        var member = new AppUser() 
        { 
            Username = "Member",
            Password = "password",
            UserRole = (int) UserRole.Member
        };
        var twoFactor = new TwoFactor()
        {
            Name = "Test",
            Secret = "Something"u8.ToArray(),
            TwoFactorMethod = (int) TwoFactorMethod.Totp
        };
        
        // Act
        var id = await repo.CreateAsync(member);
        var users = await repo.GetListAsync();
        twoFactor.UserId = id;
        await repo.CreateTwoFactorMethodAsync(twoFactor);
        await repo.DeleteAsync(id);
        var getAllForUser = await repo.GetTwoFactorMethodsForUserAsync(id);
        
        // Assert
        getAllForUser.Should().BeNullOrEmpty();
    }
    
    [Fact]
    public async Task DeletingTwoFa_Should_Delete()
    {
        // Arrange
        var repo = repositoryProvider.UserRepository;
        var member = new AppUser() 
        { 
            Username = "Member",
            Password = "password",
            UserRole = (int) UserRole.Member
        };
        var twoFactor = new TwoFactor()
        {
            Name = "Test",
            Secret = "Something"u8.ToArray(),
            TwoFactorMethod = (int) TwoFactorMethod.Totp
        };
        
        // Act
        var id = await repo.CreateAsync(member);
        twoFactor.UserId = id;
        var twoFaId = await repo.CreateTwoFactorMethodAsync(twoFactor);
        await repo.DeleteTwoFactorAsync(twoFaId);
        var getAllForUser = await repo.GetTwoFactorMethodsForUserAsync(id);
        
        // Assert
        getAllForUser.Should().BeNullOrEmpty();
    }

    [Fact]
    public async Task TwoFactorUserFK_ShouldBeEnforced()
    {
        // Arrange
        var repo = repositoryProvider.UserRepository;
        var twoFactor = new TwoFactor()
        {
            Name = "Test",
            Secret = "Something"u8.ToArray(),
            TwoFactorMethod = (int) TwoFactorMethod.Totp
        };
        
        // Act
        var act = () => repo.CreateTwoFactorMethodAsync(twoFactor);
            
        await act.Should().ThrowAsync<SqliteException>().WithMessage("SQLite Error 19: 'FOREIGN KEY constraint failed'.");
    }

    public void Dispose()
    {
        repositoryProvider.Dispose();
    }
}
