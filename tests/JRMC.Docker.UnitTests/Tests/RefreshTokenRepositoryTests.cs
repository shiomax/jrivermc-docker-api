﻿using JRMC.Docker.DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace JRMC.Docker.UnitTests.Tests;

public class RefreshTokenRepositoryTests : IDisposable
{
    private readonly RepositoryProvider repositoryProvider = new();

    [Fact]
    public async Task Create_ShouldInsert_Token()
    {
        // Arrange
        var repository = repositoryProvider.RefreshTokenRepository;
        var newRefreshToken = new RefreshToken()
        {
            CreationTime = DateTime.UtcNow.RemoveMilliseconds(),
            ExpiryTime = DateTime.UtcNow.AddHours(6).RemoveMilliseconds(),
            JwtId = "DummyJwtId",
            Token = "SomeToken",
            UserId = 47
        };

        // Act
        var id = await repository.CreateAsync(newRefreshToken);
        var inserted = await repository.GetByIdAsync(id);
        newRefreshToken.Id = id;

        // Assert
        newRefreshToken.Should().BeEquivalentTo(inserted);
    }

    [Fact]
    public async Task Find_ShouldFindToken_IfItExists()
    {
        // Arrange
        var repository = repositoryProvider.RefreshTokenRepository;
        var newRefreshToken = new RefreshToken()
        {
            CreationTime = DateTime.UtcNow.RemoveMilliseconds(),
            ExpiryTime = DateTime.UtcNow.AddHours(6).RemoveMilliseconds(),
            JwtId = "DummyJwtId",
            Token = "SomeToken",
            UserId = 47
        };

        // Act
        await repository.CreateAsync(newRefreshToken);
        var shouldExist = await repository.FindAsync("SomeToken");
        var shouldNotExist = await repository.FindAsync("something else");

        // Assert
        shouldExist.Should().NotBeNull();
        shouldNotExist.Should().BeNull();
    }

    [Fact]
    public async Task Delete_Should_DeleteEntry()
    {
        // Arrange
        var repository = repositoryProvider.RefreshTokenRepository;
        var token1 = new RefreshToken()
        {
            CreationTime = DateTime.UtcNow.RemoveMilliseconds(),
            ExpiryTime = DateTime.UtcNow.AddHours(6).RemoveMilliseconds(),
            JwtId = "DummyJwtId",
            Token = "SomeToken",
            UserId = 47
        };
        var token2 = new RefreshToken()
        {
            CreationTime = DateTime.UtcNow.RemoveMilliseconds(),
            ExpiryTime = DateTime.UtcNow.AddHours(6).RemoveMilliseconds(),
            JwtId = "DummyJwtId",
            Token = "SomeToken2",
            UserId = 47
        };

        // Act
        var id1 = await repository.CreateAsync(token1);
        var id2 = await repository.CreateAsync(token2);
        var deleteFirst = await repository.DeleteAsync(id1);
        var deleteNonExistent = await repository.DeleteAsync(4711);
        var getFirst = await repository.GetByIdAsync(id1);
        var getSecond = await repository.GetByIdAsync(id2);

        // Assert
        deleteFirst.Should().BeTrue();
        deleteNonExistent.Should().BeFalse();
        getSecond.Should().NotBeNull();
        getFirst.Should().BeNull();
    }

    [Fact]
    public async Task FindAllExpired_Should_ReturnExpiredTokens()
    {
        // Arrange
        var nowTime = DateTime.UtcNow.RemoveMilliseconds();
        var repository = repositoryProvider.RefreshTokenRepository;
        var expiredTokens = Enumerable.Range(1, 10).Select(i => new RefreshToken() {
            CreationTime = nowTime,
            ExpiryTime = nowTime.AddSeconds(-1).RemoveMilliseconds(),
            JwtId = "DummyJwtId",
            Token = $"SomeToken-{i}",
            UserId = 47
        }).ToList();
        var notExpiredToken = new RefreshToken()
        {
            CreationTime = nowTime,
            ExpiryTime = nowTime.AddMinutes(1).RemoveMilliseconds(),
            JwtId = "DummyJwtId",
            Token = $"SomeTokenNotDeleted",
            UserId = 47
        };

        // Act
        var id = await repository.CreateAsync(notExpiredToken);
        foreach (var cur in expiredTokens)
            await repository.CreateAsync(cur);
        var deletedCount = await repository.DeleteAllExpiredAsync(nowTime);
        var deletedAgainCount = await repository.DeleteAllExpiredAsync(nowTime);
        var findNotExpired = await repository.GetByIdAsync(id);

        // Assert
        deletedCount.Should().Be(10);
        deletedAgainCount.Should().Be(0);
        findNotExpired.Should().NotBeNull();
    }

    [Fact]
    public async Task ClearAll_ShouldDelete_AllTokens() 
    {
        // Arrange
        var repository = repositoryProvider.RefreshTokenRepository;
        var bunchOfTokens = Enumerable.Range(1, 10).Select(i => new RefreshToken() {
            CreationTime = DateTime.UtcNow.RemoveMilliseconds(),
            ExpiryTime = DateTime.UtcNow.AddMinutes(10).RemoveMilliseconds(),
            JwtId = "DummyJwtId",
            Token = $"SomeToken-{i}",
            UserId = 47
        }).ToList();

        foreach(var cur in bunchOfTokens)
            await repository.CreateAsync(cur);

        // Act
        var resultsBeforeDeletion = new List<RefreshToken?>();
        foreach(var cur in  bunchOfTokens)
            resultsBeforeDeletion.Add(await repository.FindAsync(cur.Token));

        await repository.ClearAllAsync();

        var resultsAfterDeletion = new List<RefreshToken>();
        foreach(var cur in  bunchOfTokens) {
            var result = await repository.FindAsync(cur.Token);
            if(result is not null) resultsAfterDeletion.Add(result);
        }

        // Assert
        resultsBeforeDeletion.Should().NotContainNulls();
        resultsAfterDeletion.Should().BeEmpty();
    }

    [Fact]
    public async Task Delete_ExpiredOnEmptyDb_ShouldNotFail()
    {
        // Arrange
        var repository = repositoryProvider.RefreshTokenRepository;
        
        // Act
        var deletedCount = await repository.DeleteAllExpiredAsync(DateTime.UtcNow);
        
        // Assert
        deletedCount.Should().Be(0);
    }

    public void Dispose()
    {
        repositoryProvider.Dispose();
    }
}
