using System;
using Dapper;
using JRMC.Docker.DataAccess.Entities;
using Microsoft.Data.Sqlite;

namespace JRMC.Docker.UnitTests.Tests;

public class Migrate20230520Tests : IDisposable
{
    private readonly RepositoryProvider repositoryProvider = new("2023_05_20_Dapper.sqlite");

    [Fact]
    public async Task User_Should_Still_Exist()
    {
        // Act
        var user = await repositoryProvider.UserRepository.FindAsync("max");
        var usersCount = await repositoryProvider.UserRepository.CountAsync();
        
        // Assert
        user.Should().NotBeNull();
        usersCount.Should().Be(1);
        user?.Password.Should().Be("password");
    }

    [Fact]
    public async Task Refresh_Token_Repo_Should_Work()
    {
        // Arrange
        var refreshToken = new RefreshToken()
        {
            Token = "Some Token",
            ExpiryTime = new DateTime(2022, 10, 10, 0, 0, 0, DateTimeKind.Utc),
            CreationTime = new DateTime(2022, 10, 10, 0, 0, 0, DateTimeKind.Utc),
            JwtId = "Some Id",
            UserId = 1
        };

        // Act
        int countBefore;
        await using (var connection = new SqliteConnection(repositoryProvider.DatabaseSettings.ConnectionString))
        {
            countBefore = await connection.ExecuteScalarAsync<int>("SELECT count(*) FROM RefreshToken");
        }
        
        refreshToken.Id = await repositoryProvider.RefreshTokenRepository.CreateAsync(refreshToken);
        var insertedToken = await repositoryProvider.RefreshTokenRepository.FindAsync("Some Token");

        // Assert
        insertedToken.Should().BeEquivalentTo(refreshToken);
        countBefore.Should().Be(0);
    }

    public void Dispose()
    {
        repositoryProvider.Dispose();
    }
}