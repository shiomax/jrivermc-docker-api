using System;
using System.Linq;
using JRMC.Docker.WebApi.Contracts.Requests.Auth;
using JRMC.Docker.WebApi.Contracts.Responses.Auth;
using JRMC.Docker.WebApi.IntegrationTests.Tools.Mocks;
using JRMC.Docker.WebApi.Services;
using JRMC.Docker.WebApi.Settings;
using Microsoft.Extensions.DependencyInjection;

namespace JRMC.Docker.WebApi.IntegrationTests.Tests;

public class TwoFactorDisabledTests : IntegrationTestBase
{
    protected override void ConfigureServices(IServiceCollection services)
    {
        var registeredClock = services.FirstOrDefault(x => x.ServiceType == typeof(ISystemClock));
        if (registeredClock is null) throw new Exception($"Expected {nameof(ISystemClock)} to be registered");
        services.Remove(registeredClock);
        services.AddSingleton<ISystemClock, TestSystemClock>();

        var registeredSettings = services.FirstOrDefault(x => x.ServiceType == typeof(TwoFactorSettings));
        if (registeredSettings is null) throw new Exception($"Expected {nameof(TwoFactorSettings)} to be registered");
        services.Remove(registeredSettings);
        services.AddSingleton(new TwoFactorSettings()
        {
            Enabled = false
        });
    }
    
    [Fact]
    public async Task TwoFactor_ShouldNotBeRequired_IfDisabledGlobally()
    {
        // Arrange
        await RegisterAppUserAsync();
        await LoginAsync();
        await Client.PostAsJsonAsync(ApiRoutes.Auth.PostTwoFactorEnabled, new PostTwoFactorEnabledRequest()
        {
            Enabled = true
        }, JsonSerializerOptions);

        await Client.PostAsJsonAsync(ApiRoutes.Auth.PostCreateTwoFactor, new PostCreateTwoFactorRequest()
        {
            Name = "Test"
        }, JsonSerializerOptions);
        Logout();

        // Act
        var responseRaw = await Client.PostAsJsonAsync(ApiRoutes.Auth.PostLogin, new LoginRequest()
        {
            Username = Username,
            Password = Password
        }, JsonSerializerOptions);
        var response = await responseRaw.Content.ReadFromJsonAsync<LoginResponse>(JsonSerializerOptions);

        // Assert
        responseRaw.StatusCode.Should().Be(HttpStatusCode.OK);
        response.Should().NotBeNull();
        response!.Token.Should().NotBeNullOrEmpty();
        response!.RefreshToken.Should().NotBeNullOrEmpty();
        response!.TwoFactorRequired.Should().BeFalse();
    }
}