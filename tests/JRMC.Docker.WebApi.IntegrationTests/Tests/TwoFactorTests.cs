using System;
using System.Collections.Generic;
using System.Linq;
using JRMC.Docker.WebApi.Contracts.Requests.Auth;
using JRMC.Docker.WebApi.Contracts.Responses.Auth;
using JRMC.Docker.WebApi.IntegrationTests.Tools.Mocks;
using JRMC.Docker.WebApi.Services;
using JRMC.Docker.WebApi.Utils;
using Microsoft.Extensions.DependencyInjection;
using OtpNet;

namespace JRMC.Docker.WebApi.IntegrationTests.Tests;

public class TwoFactorTests : IntegrationTestBase
{
    protected override void ConfigureServices(IServiceCollection services)
    {
        var registeredClock = services.FirstOrDefault(x => x.ServiceType == typeof(ISystemClock));
        if (registeredClock is null) throw new Exception($"Expected {nameof(ISystemClock)} to be registered");
        services.Remove(registeredClock);
        services.AddSingleton<ISystemClock, TestSystemClock>();
    }

    [Fact]
    public async Task TwoFactor_ShouldBeRequired_IfEnabled()
    {
        // Arrange
        await RegisterAppUserAsync();
        await LoginAsync();
        await EnableTwoFactorAsync("Test");
        Logout();

        // Act
        var responseRaw = await Client.PostAsJsonAsync(ApiRoutes.Auth.PostLogin, new LoginRequest()
        {
            Username = Username,
            Password = Password
        }, JsonSerializerOptions);
        var response = await responseRaw.Content.ReadFromJsonAsync<LoginResponse>();

        // Assert
        responseRaw.StatusCode.Should().Be(HttpStatusCode.OK);
        response.Should().NotBeNull();
        response!.Token.Should().BeNullOrEmpty();
        response!.RefreshToken.Should().BeNullOrEmpty();
        response!.TwoFactorRequired.Should().BeTrue();
    }
    
    [Fact]
    public async Task TwoFactor_ShouldNotBeRequired_IfDisabledOnUser()
    {
        // Arrange
        await RegisterAppUserAsync();
        await LoginAsync();
        await Client.PostAsJsonAsync(ApiRoutes.Auth.PostCreateTwoFactor, new PostCreateTwoFactorRequest()
        {
            Name = "Test"
        }, JsonSerializerOptions);
        Logout();

        // Act
        var responseRaw = await Client.PostAsJsonAsync(ApiRoutes.Auth.PostLogin, new LoginRequest()
        {
            Username = Username,
            Password = Password
        }, JsonSerializerOptions);
        var response = await responseRaw.Content.ReadFromJsonAsync<LoginResponse>(JsonSerializerOptions);

        // Assert
        responseRaw.StatusCode.Should().Be(HttpStatusCode.OK);
        response.Should().NotBeNull();
        response!.Token.Should().NotBeNullOrEmpty();
        response!.RefreshToken.Should().NotBeNullOrEmpty();
        response!.TwoFactorRequired.Should().BeFalse();
    }

    [Fact]
    public async Task ValidTwoFactor_ShouldBe_Accepted()
    {
        // Arrange
        await RegisterAppUserAsync();
        await LoginAsync();
        var totp = await EnableTwoFactorAsync("Test");
        Logout();
        var time = new DateTime(2022, 10, 24, 0, 0, 0, DateTimeKind.Utc);
        SetTime(time);

        // Act
        var code = totp.ComputeTotp(time);
        var responseRaw = await Client.PostAsJsonAsync(ApiRoutes.Auth.PostLogin, new LoginRequest()
        {
            Username = Username,
            Password = Password,
            TwoFactorCode = code
        }, JsonSerializerOptions);
        var response = await responseRaw.Content.ReadFromJsonAsync<LoginResponse>(JsonSerializerOptions);
        
        // Assert
        responseRaw.StatusCode.Should().Be(HttpStatusCode.OK);
        response!.Token.Should().NotBeNullOrEmpty();
        response!.RefreshToken.Should().NotBeNullOrEmpty();
    }

    [Fact]
    public async Task TwoFactor_ShouldBeDisabled_WhenDeletingLastEntry()
    {
        // Arrange
        await RegisterAppUserAsync();
        await LoginAsync();
        await EnableTwoFactorAsync("Test");
        
        // Act
        var responseTwoFactorsRaw = await Client.GetAsync(ApiRoutes.Auth.GetTwoFactors);
        var responseTwoFactors = await responseTwoFactorsRaw.Content.ReadFromJsonAsync<GetTwoFactorResponse>(JsonSerializerOptions);
        
        var responseEnabled1Raw = await Client.GetAsync(ApiRoutes.Auth.GetTwoFactorEnabled);
        var responseEnabled1 = await responseEnabled1Raw.Content.ReadFromJsonAsync<GetTwoFactorEnabledResponse>(JsonSerializerOptions);

        await Client.DeleteAsync(ApiRoutes.Auth.DeleteTwoFactor.Replace("{id}", responseTwoFactors!.Factors.First().Id.ToString()));
        
        var responseEnabled2Raw = await Client.GetAsync(ApiRoutes.Auth.GetTwoFactorEnabled);
        var responseEnabled2 = await responseEnabled2Raw.Content.ReadFromJsonAsync<GetTwoFactorEnabledResponse>(JsonSerializerOptions);
        

        // Assert
        responseTwoFactorsRaw.StatusCode.Should().Be(HttpStatusCode.OK);
        responseEnabled1!.Enabled.Should().BeTrue();
        responseEnabled2!.Enabled.Should().BeFalse();
    }

    [Fact]
    public async Task InvalidTwoFactors_ShouldBeRejected()
    {
        // Arrange
        await RegisterAppUserAsync();
        await LoginAsync();
        var totp = await EnableTwoFactorAsync("Test");
        Logout();
        var time = new DateTime(2022, 10, 24, 0, 0, 0, DateTimeKind.Utc);
        SetTime(time);

        // Act
        var codeBefore = totp.ComputeTotp(time.Subtract(TimeSpan.FromSeconds(30)));
        var responseBeforeRaw = await Client.PostAsJsonAsync(ApiRoutes.Auth.PostLogin, new LoginRequest()
        {
            Username = Username,
            Password = Password,
            TwoFactorCode = codeBefore
        }, JsonSerializerOptions);
        var responseBefore = await responseBeforeRaw.Content.ReadFromJsonAsync<LoginResponse>(JsonSerializerOptions);
        
        var codeAfter = totp.ComputeTotp(time.Add(TimeSpan.FromSeconds(30)));
        var responseAfterRaw = await Client.PostAsJsonAsync(ApiRoutes.Auth.PostLogin, new LoginRequest()
        {
            Username = Username,
            Password = Password,
            TwoFactorCode = codeAfter
        }, JsonSerializerOptions);
        var responseAfter = await responseAfterRaw.Content.ReadFromJsonAsync<LoginResponse>(JsonSerializerOptions);
        
        // Assert
        codeAfter.Should().NotBe(codeBefore).And.NotBe("066374");
        responseAfterRaw.StatusCode.Should().Be(HttpStatusCode.OK);
        responseAfter!.Token.Should().BeNullOrEmpty();
        responseAfter.RefreshToken.Should().BeNullOrEmpty();
        responseAfter.TwoFactorRequired.Should().BeTrue();
        responseBeforeRaw.StatusCode.Should().Be(HttpStatusCode.OK);
        responseBefore!.Token.Should().BeNullOrEmpty();
        responseBefore.RefreshToken.Should().BeNullOrEmpty();
        responseBefore.TwoFactorRequired.Should().BeTrue();
    }

    [Fact]
    public async Task TwoFactor_ShouldNotEnable_WhenNoneAreConfigured()
    {
        // Arrange
        await RegisterAppUserAsync();
        await LoginAsync();
        
        // Act
        var responseSetRaw = await Client.PostAsJsonAsync(ApiRoutes.Auth.PostTwoFactorEnabled, new PostTwoFactorEnabledRequest()
        {
            Enabled = true
        }, JsonSerializerOptions);
        var responseSet = await responseSetRaw.Content.ReadFromJsonAsync<PostTwoFactorEnabledResponse>(JsonSerializerOptions);
        var responseEnabled1Raw = await Client.GetAsync(ApiRoutes.Auth.GetTwoFactorEnabled);
        var responseEnabled1 = await responseEnabled1Raw.Content.ReadFromJsonAsync<GetTwoFactorEnabledResponse>(JsonSerializerOptions);
        
        // Assert
        responseEnabled1!.Enabled.Should().BeFalse();
        responseSet!.Success.Should().BeFalse();
    }

    private void SetTime(DateTime time) => (ServiceProvider.GetRequiredService<ISystemClock>() as TestSystemClock)!.UtcNow = time;

    private async Task<Totp> EnableTwoFactorAsync(string name)
    {
        var response = await Client.PostAsJsonAsync(ApiRoutes.Auth.PostCreateTwoFactor, new PostCreateTwoFactorRequest()
        {
            Name = name
        }, JsonSerializerOptions);
        var secretBase32 = (await response.Content.ReadFromJsonAsync<PostCreateTwoFactorResponse>(JsonSerializerOptions))!.SecretBase32;
        
        await Client.PostAsJsonAsync(ApiRoutes.Auth.PostTwoFactorEnabled, new PostTwoFactorEnabledRequest()
        {
            Enabled = true
        });

        var secret = Base32Encoder.Decode(secretBase32);
        return new Totp(secret);
    }
}