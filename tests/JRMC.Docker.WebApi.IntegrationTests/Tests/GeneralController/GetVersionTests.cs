using System.Linq;
using JRMC.Docker.WebApi.Contracts.Responses.General;
using JRMC.Docker.WebApi.IntegrationTests.Tools.Mocks;
using JRMC.Docker.WebApi.Services.Version.Contracts;
using Microsoft.Extensions.DependencyInjection;

namespace JRMC.Docker.WebApi.IntegrationTests.Tests.GeneralController;

public class GetVersionTests : IntegrationTestBase
{
    protected override void ConfigureServices(IServiceCollection services) 
    { 
        var registeredProvider = services.FirstOrDefault(x => x.ServiceType == typeof(IVersionFileProvider));
        if(registeredProvider is not null) services.Remove(registeredProvider);
        services.AddSingleton<IVersionFileProvider, InMemoryVersionFileProvider>();
    }

    [Fact]
    public async Task GetVersion_ShouldParse_FileWithoutErrors()
    {
        // Arrange
        await RegisterAppUserAsync();
        await LoginAsync();
        
        // Act
        var response = await Client.GetAsync(ApiRoutes.General.GetVersion);
        var getVersionResponse = await response.Content.ReadFromJsonAsync<GetVersionResponse>(JsonSerializerOptions);
        
        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);
        getVersionResponse!.DebUrl.Should().Be("repository");
        getVersionResponse.ImageVersion.Should().Be("v0.0.2");
        getVersionResponse.UsesRepository.Should().BeTrue();
        getVersionResponse.MediaCenterTag.Should().Be("latest");
    }

    private const string FileWithExtraContents =
        """
             IMAGE_VERSION v0.0.2 Something Here
        DEB_URL repository ___
           JRIVER_TAG latest
        """;
    
    [Fact]
    public async Task GetVersion_ShouldParse_FileWithExtraContents()
    {
        // Arrange
        var fileProvider = (ServiceProvider.GetService<IVersionFileProvider>() as InMemoryVersionFileProvider)!;
        fileProvider.FileContents = FileWithExtraContents;
        await RegisterAppUserAsync();
        await LoginAsync();
        
        // Act
        var response = await Client.GetAsync(ApiRoutes.General.GetVersion);
        var getVersionResponse = await response.Content.ReadFromJsonAsync<GetVersionResponse>(JsonSerializerOptions);
        
        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);
        getVersionResponse!.DebUrl.Should().Be("repository");
        getVersionResponse.ImageVersion.Should().Be("v0.0.2");
        getVersionResponse.UsesRepository.Should().BeTrue();
        getVersionResponse.MediaCenterTag.Should().Be("latest");
    }

    [Fact]
    public async Task GetVersion_ShouldReturnDefaults_IfFileIsNotFound()
    {
        // Arrange
        var fileProvider = (ServiceProvider.GetService<IVersionFileProvider>() as InMemoryVersionFileProvider)!;
        fileProvider.Exists = false;
        await RegisterAppUserAsync();
        await LoginAsync();
        
        // Act
        var response = await Client.GetAsync(ApiRoutes.General.GetVersion);
        var getVersionResponse = await response.Content.ReadFromJsonAsync<GetVersionResponse>(JsonSerializerOptions);
        
        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);
        getVersionResponse!.DebUrl.Should().Be("unknown");
        getVersionResponse.ImageVersion.Should().Be("unknown");
        getVersionResponse.UsesRepository.Should().BeTrue();
        getVersionResponse.MediaCenterTag.Should().Be("unknown");
    }

    private const string FileWithDebUrl =
        """
        IMAGE_VERSION v0.1.0
        DEB_URL https://url.com/0.0.2.deb
        JRIVER_TAG stable
        """;
    
    
    [Fact]
    public async Task GetVersion_WithUrl_ShouldReturnUrl()
    {
        // Arrange
        var fileProvider = (ServiceProvider.GetService<IVersionFileProvider>() as InMemoryVersionFileProvider)!;
        fileProvider.FileContents = FileWithDebUrl;
        await RegisterAppUserAsync();
        await LoginAsync();
        
        // Act
        var response = await Client.GetAsync(ApiRoutes.General.GetVersion);
        var getVersionResponse = await response.Content.ReadFromJsonAsync<GetVersionResponse>(JsonSerializerOptions);
        
        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);
        getVersionResponse!.DebUrl.Should().Be("https://url.com/0.0.2.deb");
        getVersionResponse.ImageVersion.Should().Be("v0.1.0");
        getVersionResponse.UsesRepository.Should().BeFalse();
        getVersionResponse.MediaCenterTag.Should().Be("stable");
    }
}