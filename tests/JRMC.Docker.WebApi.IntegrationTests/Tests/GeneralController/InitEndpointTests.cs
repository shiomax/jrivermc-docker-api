using System;
using System.Linq;
using JRMC.Docker.WebApi.Contracts.Responses.General;
using JRMC.Docker.WebApi.Settings;
using Microsoft.Extensions.DependencyInjection;

namespace JRMC.Docker.WebApi.IntegrationTests.Tests.GeneralController;

public class InitEndpointTests : IntegrationTestBase 
{
    protected override void ConfigureServices(IServiceCollection services) 
    { 
        var registeredSettings = services.FirstOrDefault(x => x.ServiceType == typeof(InitSettings));
        if (registeredSettings is null) throw new Exception($"Expected {nameof(InitSettings)} to be registered");
        services.Remove(registeredSettings);
        services.AddSingleton(new InitSettings() {
            InitialHeight = 720,
            InitialWidth = 1080
        });
    } 


    [Fact]
    public async Task CanAdopt_Should_ReturnTrue_IfNoUserExists()
    {
        // Act
        var response = await Client.GetAsync(ApiRoutes.General.GetInit);
        var getInitResponse = await response.Content.ReadFromJsonAsync<GetInitResponse>(JsonSerializerOptions);

        // Assert
        getInitResponse.Should().NotBeNull();
        getInitResponse!.AuthRequired.Should().BeTrue();
        getInitResponse!.CanAdopt.Should().BeTrue();
        getInitResponse.InitialHeight.Should().Be(720);
        getInitResponse.InitialWidth.Should().Be(1080);
        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }


    [Fact]
    public async Task CanAdopt_Should_ReturnFalse_IfUserExists()
    {
        // Arrange
        await RegisterAppUserAsync();

        // Act
        var response = await Client.GetAsync(ApiRoutes.General.GetInit);
        var getInitResponse = await response.Content.ReadFromJsonAsync<GetInitResponse>(JsonSerializerOptions);

        // Assert
        getInitResponse.Should().NotBeNull();
        getInitResponse!.AuthRequired.Should().BeTrue();
        getInitResponse!.CanAdopt.Should().BeFalse();
        getInitResponse.InitialHeight.Should().Be(720);
        getInitResponse.InitialWidth.Should().Be(1080);
        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }
}