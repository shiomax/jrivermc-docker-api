﻿using JRMC.Docker.DataAccess.Contracts;
using JRMC.Docker.DataAccess.Entities;
using JRMC.Docker.WebApi.Contracts.Requests.Auth;
using JRMC.Docker.WebApi.Contracts.Responses.Auth;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using Bogus;
using Dapper;
using JRMC.Docker.WebApi.Services.Auth.Contracts;
using Microsoft.Data.Sqlite;

namespace JRMC.Docker.WebApi.IntegrationTests.Tests;

public class AuthControllerTests : IntegrationTestBase
{
    [Fact]
    public async Task Adopt_Should_WorkOnce_IfNoUserExists()
    {
        // Act
        var response = await Client.PostAsJsonAsync(ApiRoutes.Auth.PostAdopt, new AdoptRequest() { 
            Username = Username,
            Password = Password
        });
        var adoptResponse = await response.Content.ReadFromJsonAsync<AdoptResponse>();

        // Assert
        adoptResponse.Should().NotBeNull();
        adoptResponse!.Success.Should().BeTrue();
        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }

    [Fact]
    public async Task Adopt_Should_NotWork_IfUserExists()
    {
        // Arrange
        await RegisterAppUserAsync();

        // Act
        var response = await Client.PostAsJsonAsync(ApiRoutes.Auth.PostAdopt, new AdoptRequest()
        {
            Username = Username,
            Password = Password
        });
        
        var adoptResponse = await response.Content.ReadFromJsonAsync<AdoptResponse>();

        // Assert
        adoptResponse.Should().NotBeNull();
        adoptResponse!.Success.Should().BeFalse();
        response.StatusCode.Should().Be(HttpStatusCode.OK);
    }

    [Fact]
    public async Task ChangingPassword_Without_Login_Should_BeForbidden()
    {
        // Act
        var response = await Client.PostAsJsonAsync(ApiRoutes.Auth.PostChangePassword, new ChangePasswordRequest() { 
            NewPassword = "newPassword",
            Password = "password"
        });

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.Unauthorized);
    }

    [Fact]
    public async Task ChangingUsername_Without_Login_Should_BeForbidden()
    {
        // Act
        var response = await Client.PostAsJsonAsync(ApiRoutes.Auth.PostChangeUsername, new ChangeUsernameRequest()
        {
            NewUsername = "newUsername",
            Password = "password"
        });

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.Unauthorized);
    }

    [Fact]
    public async Task ChangingPassword_Should_ChangePassword()
    {
        // Arrange
        await RegisterAppUserAsync();
        await LoginAsync();

        // Act
        var response = await Client.PostAsJsonAsync(ApiRoutes.Auth.PostChangePassword, new ChangePasswordRequest() { 
            Password = Password,
            NewPassword = "newPassword"
        });
        var changePasswordResponse = await response.Content.ReadFromJsonAsync<ChangePasswordResponse>();

        Logout();
        var oldPasswordResponseRaw = await Client.PostAsJsonAsync(ApiRoutes.Auth.PostLogin, new LoginRequest()
        {
            Password = Password,
            Username = Username
        });
        var oldPasswordResponse = await oldPasswordResponseRaw.Content.ReadFromJsonAsync<LoginResponse>();
        var newPasswordResponseRaw = await Client.PostAsJsonAsync(ApiRoutes.Auth.PostLogin, new LoginRequest()
        {
            Password = "newPassword",
            Username = Username
        });
        var newPasswordResponse = await newPasswordResponseRaw.Content.ReadFromJsonAsync<LoginResponse>();

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);
        changePasswordResponse.Should().NotBeNull();
        changePasswordResponse!.Success.Should().BeTrue();
        oldPasswordResponseRaw.StatusCode.Should().Be(HttpStatusCode.OK);
        newPasswordResponseRaw.StatusCode.Should().Be(HttpStatusCode.OK);
        oldPasswordResponse.Should().NotBeNull();
        newPasswordResponse.Should().NotBeNull();
        oldPasswordResponse!.Success.Should().BeFalse();
        newPasswordResponse!.Success.Should().BeTrue();
        oldPasswordResponse.Errors.Should().NotBeEmpty();
        newPasswordResponse.Errors.Should().BeNullOrEmpty();
        oldPasswordResponse.Token.Should().BeNullOrEmpty();
        newPasswordResponse.Token.Should().NotBeNullOrEmpty();
    }

    [Fact]
    public async Task ChangingUsername_Should_ChangePassword()
    {
        // Arrange
        await RegisterAppUserAsync();
        await LoginAsync();

        // Act
        var response = await Client.PostAsJsonAsync(ApiRoutes.Auth.PostChangeUsername, new ChangeUsernameRequest()
        {
            Password = Password,
            NewUsername = "newUsername"
        });
        var changeusernameResponse = await response.Content.ReadFromJsonAsync<ChangeUsernameResponse>();

        Logout();
        var oldPasswordResponseRaw = await Client.PostAsJsonAsync(ApiRoutes.Auth.PostLogin, new LoginRequest()
        {
            Password = Password,
            Username = Username
        });
        var oldPasswordResponse = await oldPasswordResponseRaw.Content.ReadFromJsonAsync<LoginResponse>();
        var newPasswordResponseRaw = await Client.PostAsJsonAsync(ApiRoutes.Auth.PostLogin, new LoginRequest()
        {
            Password = Password,
            Username = "newUsername"
        });
        var newPasswordResponse = await newPasswordResponseRaw.Content.ReadFromJsonAsync<LoginResponse>();

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);
        changeusernameResponse.Should().NotBeNull();
        changeusernameResponse!.Success.Should().BeTrue();
        oldPasswordResponseRaw.StatusCode.Should().Be(HttpStatusCode.OK);
        newPasswordResponseRaw.StatusCode.Should().Be(HttpStatusCode.OK);
        oldPasswordResponse.Should().NotBeNull();
        newPasswordResponse.Should().NotBeNull();
        oldPasswordResponse!.Success.Should().BeFalse();
        newPasswordResponse!.Success.Should().BeTrue();
        oldPasswordResponse.Errors.Should().NotBeEmpty();
        newPasswordResponse.Errors.Should().BeNullOrEmpty();
        oldPasswordResponse.Token.Should().BeNullOrEmpty();
        newPasswordResponse.Token.Should().NotBeNullOrEmpty();
    }

    [Fact]
    public async Task ChangePassword_ShouldFail_IfPasswordIsWrong()
    {
        // Arrange
        await RegisterAppUserAsync();
        await LoginAsync();

        // Act
        var response = await Client.PostAsJsonAsync(ApiRoutes.Auth.PostChangePassword, new ChangePasswordRequest()
        {
            Password = "wrongPassword",
            NewPassword = "newPassword"
        });
        var changePasswordResponse = await response.Content.ReadFromJsonAsync<ChangePasswordResponse>();

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);
        changePasswordResponse.Should().NotBeNull();
        changePasswordResponse!.Success.Should().BeFalse();
        changePasswordResponse!.Errors.Should().NotBeEmpty();
    }

    [Fact]
    public async Task ChangeUserName_ShouldFail_IfPasswordIsWrong()
    {
        // Arrange
        await RegisterAppUserAsync();
        await LoginAsync();

        // Act
        var response = await Client.PostAsJsonAsync(ApiRoutes.Auth.PostChangeUsername, new ChangeUsernameRequest()
        {
            Password = "wrongPassword",
            NewUsername = "newUsername"
        });
        var changeUsernameResponse = await response.Content.ReadFromJsonAsync<ChangeUsernameResponse>();

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.OK);
        changeUsernameResponse.Should().NotBeNull();
        changeUsernameResponse!.Success.Should().BeFalse();
        changeUsernameResponse!.Errors.Should().NotBeEmpty();
    }

    [Fact]
    public async Task CreateUser_ShouldCreateUser_AsOwner()
    {
        // Arrange
        await RegisterAppUserAsync();
        await LoginAsync();
        var repository = ServiceProvider.GetRequiredService<IUserRepository>();

        // Act
        var response = await Client.PostAsJsonAsync(ApiRoutes.Auth.PostCreateUser, new CreateUserRequest()
        {
            Username = "NewUser",
            Password = "NewPassword",
            UserRole = UserRole.Member
        });
        var createUserRespone = await response.Content.ReadFromJsonAsync<CreateUserResponse>();
        var users = await repository.GetListAsync();

        // Assert
        createUserRespone.Should().NotBeNull();
        createUserRespone!.Success.Should().BeTrue();
        users.Should().HaveCount(2);
    }

    [Fact]
    public async Task CreateUser_AsMember_ShouldFail()
    {
        // Arrange
        await RegisterAppUserAsync(UserRole.Member);
        await LoginAsync();

        // Act
        var response = await Client.PostAsJsonAsync(ApiRoutes.Auth.PostCreateUser, new CreateUserRequest()
        {
            Username = "NewUser",
            Password = "NewPassword",
            UserRole = UserRole.Member
        });

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.Forbidden);
    }

    [Fact]
    public async Task Refresh_ShouldIssueNewTokens()
    {
        // Arrange
        await RegisterAppUserAsync();
        var loginResponse = await LoginAsync();

        // Act
        var response = await Client.PostAsJsonAsync(ApiRoutes.Auth.PostRefresh, new RefreshRequest()
        {
            RefreshToken = loginResponse!.RefreshToken!,
            Token = loginResponse!.Token!
        });
        var refreshResponse = await response.Content.ReadFromJsonAsync<RefreshResponse>();

        // Assert
        refreshResponse.Should().NotBeNull();
        refreshResponse!.Success.Should().BeTrue();
        refreshResponse!.Token.Should().NotBeNullOrEmpty();
        refreshResponse!.RefreshToken.Should().NotBeNullOrEmpty();
    }

    [Fact]
    public async Task Refresh_ShouldFail_WhenRefreshTokenIsWrong()
    {
        // Arrange
        await RegisterAppUserAsync();
        var loginResponse = await LoginAsync();

        // Act
        var response = await Client.PostAsJsonAsync(ApiRoutes.Auth.PostRefresh, new RefreshRequest()
        {
            RefreshToken = "Wrong refresh token",
            Token = loginResponse!.Token!
        }, JsonSerializerOptions);
        var refreshResponse = await response.Content.ReadFromJsonAsync<RefreshResponse>();

        // Assert
        refreshResponse.Should().NotBeNull();
        refreshResponse!.Success.Should().BeFalse();
        refreshResponse!.Errors.Should().HaveCount(1);
        refreshResponse!.Token.Should().BeNullOrEmpty();
        refreshResponse!.RefreshToken.Should().BeNullOrEmpty();
    }

    [Fact]
    public async Task Refresh_ShouldFail_WhenTokenIsWrong()
    {
        // Arrange
        await RegisterAppUserAsync();
        var loginResponse = await LoginAsync();

        // Act
        var response = await Client.PostAsJsonAsync(ApiRoutes.Auth.PostRefresh, new RefreshRequest()
        {
            RefreshToken = loginResponse!.RefreshToken!,
            Token = "Wrong JWT token"
        }, JsonSerializerOptions);
        var refreshResponse = await response.Content.ReadFromJsonAsync<RefreshResponse>();

        // Assert
        refreshResponse.Should().NotBeNull();
        refreshResponse!.Success.Should().BeFalse();
        refreshResponse!.Errors.Should().HaveCount(1);
        refreshResponse!.Token.Should().BeNullOrEmpty();
        refreshResponse!.RefreshToken.Should().BeNullOrEmpty();
    }

    [Fact]
    public async Task Refresh_ShouldFail_WithExpiredRefreshToken()
    {
        // Arrange
        await RegisterAppUserAsync();
        var loginResponse = await LoginAsync();
        var repo = ServiceProvider.GetRequiredService<IRefreshTokenRepository>();

        var tokenValues = GetJwtIdAndUserIdFromToken(loginResponse!.Token!);

        await repo.CreateAsync(new RefreshToken()
        {
            CreationTime = DateTime.UtcNow,
            ExpiryTime = DateTime.UtcNow.AddSeconds(-1),
            JwtId = tokenValues.JwtId,
            Token = "SomeToken",
            UserId = tokenValues.UserId
        });

        // Act
        var response = await Client.PostAsJsonAsync(ApiRoutes.Auth.PostRefresh, new RefreshRequest()
        {
            RefreshToken = "SomeToken",
            Token = loginResponse!.Token!
        }, JsonSerializerOptions);
        var refreshResponse = await response.Content.ReadFromJsonAsync<RefreshResponse>();

        // Assert
        refreshResponse!.Success.Should().BeFalse();
        refreshResponse.Errors.Should().HaveCount(1);
        refreshResponse.Errors.Should().BeEquivalentTo("RefreshToken has expired.");
    }

    [Fact]
    public async Task GetUsersAsAdmin_ShouldReturnUsers()
    {
        // Arrange
        await RegisterAppUserAsync(UserRole.Admin);
        await LoginAsync();
        
        // Act
        var responseRaw = await Client.GetAsync(ApiRoutes.Auth.GetUsers);
        var response = await responseRaw.Content.ReadFromJsonAsync<GetUsersResponse>(JsonSerializerOptions);

        // Assert
        responseRaw.StatusCode.Should().Be(HttpStatusCode.OK);
        response.Should().NotBeNull();
        response!.Users.Should().NotBeNull();
        response.Users.Should().HaveCount(1);
        response.Users!.First().UserRole.Should().Be(UserRole.Admin);
        response.Users!.First().Username.Should().Be(Username);
    }

    [Fact]
    public async Task GetUsersAsMember_ShouldBeForbidden()
    {
        // Arrange
        await RegisterAppUserAsync(UserRole.Member);
        await LoginAsync();
        
        // Act
        var response = await Client.GetAsync(ApiRoutes.Auth.GetUsers);

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.Forbidden);
    }

    [Fact]
    public async Task DeleteUserAsMember_ShouldBeForbidden()
    {
        // Arrange
        await RegisterAppUserAsync(UserRole.Member);
        await LoginAsync();
        var userId = (await GetAppUserAsync())!.Id;
        
        // Act
        var response = await Client.DeleteAsync(ApiRoutes.Auth.DeleteUser.Replace("{id}", userId.ToString()));

        // Assert
        response.StatusCode.Should().Be(HttpStatusCode.Forbidden);
    }

    [Fact]
    public async Task DeleteAsAdmin_ShouldBeAbleTo_DeleteMemberAndAdmin()
    {
        // Arrange
        await RegisterAppUserAsync(UserRole.Admin);
        await LoginAsync();

        await CreateRandomUserAsync(20, 4711);
        var repository = ServiceProvider.GetRequiredService<IUserRepository>();
        var users = await repository.GetListAsync();
        
        var member = users.First(x => x.UserRole == (int) UserRole.Member);
        var admin = users.First(x => x.UserRole == (int) UserRole.Admin && x.Username != Username);
        var self = users.First(x => x.Username == Username);
        var owner = users.Last();
        
        var settings = ServiceProvider.GetRequiredService<IDatabaseSettings>();
        await using (var connection = new SqliteConnection(settings.ConnectionString))
        {
#pragma warning disable DAP005
            await connection.ExecuteAsync("UPDATE AppUser SET UserRole = @role WHERE Id = @id", new { role = UserRole.Owner, id = owner.Id });
#pragma warning restore DAP005
        };

        // Act
        var responseDeleteMemberRaw = await Client.DeleteAsync(ApiRoutes.Auth.DeleteUser.Replace("{id}", member.Id.ToString()));
        var responseDeleteMember = await responseDeleteMemberRaw.Content.ReadFromJsonAsync<DeleteUserResponse>(JsonSerializerOptions);
        
        var responseDeleteAdminRaw = await Client.DeleteAsync(ApiRoutes.Auth.DeleteUser.Replace("{id}", admin.Id.ToString()));
        var responseDeleteAdmin = await responseDeleteAdminRaw.Content.ReadFromJsonAsync<DeleteUserResponse>(JsonSerializerOptions);
        
        var responseDeleteSelfRaw = await Client.DeleteAsync(ApiRoutes.Auth.DeleteUser.Replace("{id}", self.Id.ToString()));
        var responseDeleteSelf = await responseDeleteSelfRaw.Content.ReadFromJsonAsync<DeleteUserResponse>(JsonSerializerOptions);
        
        var responseDeleteOwnerRaw = await Client.DeleteAsync(ApiRoutes.Auth.DeleteUser.Replace("{id}", owner.Id.ToString()));
        var responseDeleteOwner = await responseDeleteOwnerRaw.Content.ReadFromJsonAsync<DeleteUserResponse>(JsonSerializerOptions);

        var usersAfter = await repository.GetListAsync();
        
        // Assert
        responseDeleteMember!.Success.Should().BeTrue();
        responseDeleteMember.Errors.Should().BeNullOrEmpty();
        responseDeleteAdmin!.Success.Should().BeTrue();
        responseDeleteAdmin.Errors.Should().BeNullOrEmpty();
        responseDeleteSelf!.Success.Should().BeTrue();
        responseDeleteSelf.Errors.Should().BeNullOrEmpty();
        responseDeleteOwner!.Success.Should().BeFalse();
        responseDeleteOwner.Errors.Should().BeEquivalentTo("You cannot delete a User with higher privileges than your own.");
        usersAfter.Should().HaveCount(18);
    }

    [Fact]
    public async Task DeleteOwner_ShouldNotBePossible()
    {
        // Arrange
        await RegisterAppUserAsync();
        await LoginAsync();
        var id = (await GetAppUserAsync())!.Id;

        // Act
        var responseDeleteRaw = await Client.DeleteAsync(ApiRoutes.Auth.DeleteUser.Replace("{id}", id.ToString()));
        var responseDelete = await responseDeleteRaw.Content.ReadFromJsonAsync<DeleteUserResponse>(JsonSerializerOptions);
        
        // Assert
        responseDelete!.Success.Should().BeFalse();
        responseDelete.Errors.Should().BeEquivalentTo("Owner cannot be deleted.");
    }

    private (string JwtId, long UserId) GetJwtIdAndUserIdFromToken(string token)
    {
        var handler = new JwtSecurityTokenHandler();
        var jwt = handler.ReadToken(token) as JwtSecurityToken;

        var userId = long.Parse(jwt!.Claims.Single(x => x.Type == "id").Value);
        var jwtId = jwt.Claims.Single(x => x.Type == JwtRegisteredClaimNames.Jti).Value;

        return (jwtId, userId);
    }

    private async Task CreateRandomUserAsync(int count, int seed)
    {
        Randomizer.Seed = new Random(seed);

        var authService = ServiceProvider.GetRequiredService<IAuthService>();

        var testUsers = new Faker<AppUser>()
            .RuleFor(x => x.Password, f => f.Random.AlphaNumeric(10))
            .RuleFor(x => x.Username, f => f.Name.FirstName())
            .RuleFor(x => x.UserRole, f => (int) f.Random.Enum(UserRole.Owner));

        foreach (var current in testUsers.Generate(count))
        {
            var response = await authService.RegisterAsync(current.Username, current.Password, (UserRole) current.UserRole);
            if (response is not { IsSucessful: true })
                throw new InvalidOperationException("Generated duplicates...");
        }
    }
}
