using System.Collections.Generic;
using System.Linq;
using JRMC.Docker.WebApi.Contracts.Requests.Auth;
using JRMC.Docker.WebApi.Contracts.Responses.Auth;
using JRMC.Docker.WebApi.Contracts.Responses.General;
using JRMC.Docker.WebApi.IntegrationTests.Tools.Mocks;
using JRMC.Docker.WebApi.Services.Version.Contracts;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace JRMC.Docker.WebApi.IntegrationTests.Tests;

public class DisabledAuthTests : IntegrationTestBase
{
    protected override void ConfigureAppConfiguration(IConfigurationBuilder builder)
    {
        // Disable Authentication
        builder.AddInMemoryCollection(new List<KeyValuePair<string, string?>>()
        {
            new("JwtSettings:Enabled", "false")
        });
        
        base.ConfigureAppConfiguration(builder);
    }
    
    protected override void ConfigureServices(IServiceCollection services) 
    { 
        var registeredProvider = services.FirstOrDefault(x => x.ServiceType == typeof(IVersionFileProvider));
        if(registeredProvider is not null) services.Remove(registeredProvider);
        services.AddSingleton<IVersionFileProvider, InMemoryVersionFileProvider>();
    }

    [Fact]
    public async Task GetVersion_ShouldWork_WithoutAuthentication()
    {
        // Act
        var rawResponse = await Client.GetAsync(ApiRoutes.General.GetVersion);
        var getVersionResponse = await rawResponse.Content.ReadFromJsonAsync<GetVersionResponse>(JsonSerializerOptions);
        
        // Assert
        rawResponse.StatusCode.Should().Be(HttpStatusCode.OK);
        getVersionResponse.Should().NotBeNull();
        getVersionResponse!.ImageVersion.Should().Be("v0.0.2");
    }

    [Fact]
    public async Task Adopt_ShouldNotBeAllowed_WhenAuthIsDisabled()
    {
        // Act
        var rawResponse = await Client.PostAsJsonAsync(ApiRoutes.Auth.PostAdopt, new AdoptRequest()
        {
            Username = "SomeUser",
            Password = "Some Password"
        });
        var adoptResponse = await rawResponse.Content.ReadFromJsonAsync<AdoptResponse>(JsonSerializerOptions);
        
        // Assert
        rawResponse.StatusCode.Should().Be(HttpStatusCode.OK);
        adoptResponse.Should().NotBeNull();
        adoptResponse!.Success.Should().BeFalse();
        adoptResponse.Errors.Should().BeEquivalentTo("Authentication is disabled.");
    }

    [Fact]
    public async Task CreateUser_ShouldNotBeAllowed_WhenAuthIsDisabled()
    {
        // Act
        var rawResponse = await Client.PostAsJsonAsync(ApiRoutes.Auth.PostCreateUser, new CreateUserRequest()
        {
            Username = "SomeUser",
            Password = "Some Password"
        });
        var createResponse = await rawResponse.Content.ReadFromJsonAsync<CreateUserResponse>(JsonSerializerOptions);
        
        // Assert
        rawResponse.StatusCode.Should().Be(HttpStatusCode.OK);
        createResponse.Should().NotBeNull();
        createResponse!.Success.Should().BeFalse();
        createResponse.Errors.Should().BeEquivalentTo("Authentication is disabled.");
    }

    [Fact]
    public async Task Login_ShouldNotBeAllowed_WhenAuthIsDisabled()
    {
        // Act
        var rawResponse = await Client.PostAsJsonAsync(ApiRoutes.Auth.PostLogin, new LoginRequest()
        {
            Username = "SomeUser",
            Password = "Some Password"
        });
        var loginResponse = await rawResponse.Content.ReadFromJsonAsync<LoginResponse>(JsonSerializerOptions);
        
        // Assert
        rawResponse.StatusCode.Should().Be(HttpStatusCode.OK);
        loginResponse.Should().NotBeNull();
        loginResponse!.Success.Should().BeFalse();
        loginResponse.Errors.Should().BeEquivalentTo("Authentication is disabled.");
    }
    
    [Fact]
    public async Task Refresh_ShouldNotBeAllowed_WhenAuthIsDisabled()
    {
        // Act
        var rawResponse = await Client.PostAsJsonAsync(ApiRoutes.Auth.PostRefresh, new RefreshRequest()
        {
            Token = "Token",
            RefreshToken = "MyRefreshToken"
        });
        var refreshResponse = await rawResponse.Content.ReadFromJsonAsync<RefreshResponse>(JsonSerializerOptions);
        
        // Assert
        rawResponse.StatusCode.Should().Be(HttpStatusCode.OK);
        refreshResponse.Should().NotBeNull();
        refreshResponse!.Success.Should().BeFalse();
        refreshResponse.Errors.Should().BeEquivalentTo("Authentication is disabled.");
    }
    
    [Fact]
    public async Task Logout_ShouldNotBeAllowed_WhenAuthIsDisabled()
    {
        // Act
        var rawResponse = await Client.PostAsJsonAsync(ApiRoutes.Auth.PostLogout, new LogoutRequest()
        {
            Token = "Token",
            RefreshToken = "MyRefreshToken"
        });
        var logoutResponse = await rawResponse.Content.ReadFromJsonAsync<LogoutResponse>(JsonSerializerOptions);
        
        // Assert
        rawResponse.StatusCode.Should().Be(HttpStatusCode.OK);
        logoutResponse.Should().NotBeNull();
        logoutResponse!.Success.Should().BeFalse();
        logoutResponse.Errors.Should().BeEquivalentTo("Authentication is disabled.");
    }
    
    [Fact]
    public async Task ChangePassword_ShouldNotBeAllowed_WhenAuthIsDisabled()
    {
        // Act
        var rawResponse = await Client.PostAsJsonAsync(ApiRoutes.Auth.PostChangePassword, new ChangePasswordRequest()
        {
            Password = "Secure123",
            NewPassword = "Secure1234!"
        });
        var changePasswordResponse = await rawResponse.Content.ReadFromJsonAsync<ChangePasswordResponse>(JsonSerializerOptions);
        
        // Assert
        rawResponse.StatusCode.Should().Be(HttpStatusCode.OK);
        changePasswordResponse.Should().NotBeNull();
        changePasswordResponse!.Success.Should().BeFalse();
        changePasswordResponse.Errors.Should().BeEquivalentTo("Authentication is disabled.");
    }
    
    [Fact]
    public async Task ChangeUsername_ShouldNotBeAllowed_WhenAuthIsDisabled()
    {
        // Act
        var rawResponse = await Client.PostAsJsonAsync(ApiRoutes.Auth.PostChangePassword, new ChangeUsernameRequest()
        {
            Password = "Secure123",
            NewUsername = "NewName"
        });
        var changeUsernameResponse = await rawResponse.Content.ReadFromJsonAsync<ChangeUsernameResponse>(JsonSerializerOptions);
        
        // Assert
        rawResponse.StatusCode.Should().Be(HttpStatusCode.OK);
        changeUsernameResponse.Should().NotBeNull();
        changeUsernameResponse!.Success.Should().BeFalse();
        changeUsernameResponse.Errors.Should().BeEquivalentTo("Authentication is disabled.");
    }
}