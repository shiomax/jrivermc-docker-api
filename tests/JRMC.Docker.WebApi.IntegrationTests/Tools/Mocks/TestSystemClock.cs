using System;
using JRMC.Docker.WebApi.Services;

namespace JRMC.Docker.WebApi.IntegrationTests.Tools.Mocks;

public class TestSystemClock : ISystemClock
{
    private DateTime? staticTime;
    
    public DateTime UtcNow
    {
        get => staticTime ?? DateTime.UtcNow;
        set => staticTime = value;
    }
}