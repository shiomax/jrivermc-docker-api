using System.IO;
using System.Text;
using JRMC.Docker.WebApi.Services.Version.Contracts;

namespace JRMC.Docker.WebApi.IntegrationTests.Tools.Mocks;

internal class InMemoryVersionFileProvider : IVersionFileProvider
{
    private const string DefaultFile =
        """
        IMAGE_VERSION v0.0.2
        DEB_URL repository
        JRIVER_TAG latest
        """;
    
    public string FileContents { get; set; } = DefaultFile;

    public Stream OpenRead()
        => new MemoryStream(Encoding.UTF8.GetBytes(FileContents));

    public bool Exists { get; set; } = true;
}