using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using JRMC.Docker.DataAccess.Contracts;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Data.Sqlite;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace JRMC.Docker.WebApi.IntegrationTests.Tools;

internal class WebApiFactory : WebApplicationFactory<Program>
{
    private readonly Action<IServiceCollection> configureServicesAction;
    private readonly Action<IConfigurationBuilder> configureAppConfigurationAction;
    private readonly string dbGuid = Guid.NewGuid().ToString();
    
    private string ConnectionString => $"Data Source={dbGuid}.sqlite";
    
    public WebApiFactory(
        Action<IServiceCollection> configureServicesAction,
        Action<IConfigurationBuilder> configureAppConfigurationAction)
    {
        this.configureServicesAction = configureServicesAction;
        this.configureAppConfigurationAction = configureAppConfigurationAction;
    }

    protected override IHost CreateHost(IHostBuilder builder)
    {
        builder.ConfigureHostConfiguration(configurationBuilder =>
        {
            configurationBuilder.AddInMemoryCollection(new List<KeyValuePair<string, string?>>()
            {
                new("DatabaseSettings:ConnectionString", ConnectionString)
            });
            configureAppConfigurationAction(configurationBuilder);
        });
        
        return base.CreateHost(builder);
    }

    protected override void ConfigureWebHost(IWebHostBuilder builder)
    {
        base.ConfigureWebHost(builder);
        
        builder.ConfigureServices(services =>
        {
            var currentSettings = services.FirstOrDefault(x => x.ServiceType == typeof(IDatabaseSettings));
            if (currentSettings is not null) services.Remove(currentSettings);
            services.AddSingleton<IDatabaseSettings>(_ => new DatabaseSettings()
            {
                ConnectionString = ConnectionString
            });
            configureServicesAction(services);
        });
    }

    protected override void Dispose(bool disposing)
    {
        base.Dispose(disposing);
        
        SqliteConnection.ClearAllPools();
        File.Delete($"{dbGuid}.sqlite");
    }
}