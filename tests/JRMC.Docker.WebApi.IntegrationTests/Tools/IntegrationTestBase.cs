﻿using JRMC.Docker.DataAccess.Entities;
using JRMC.Docker.WebApi.Contracts.Requests.Auth;
using JRMC.Docker.WebApi.Contracts.Responses.Auth;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Text.Json;
using JRMC.Docker.DataAccess.Contracts;
using JRMC.Docker.WebApi.Contracts.Serialization;
using JRMC.Docker.WebApi.Services.Auth.Contracts;
using Microsoft.Extensions.Configuration;

namespace JRMC.Docker.WebApi.IntegrationTests.Tools;

public abstract class IntegrationTestBase : IDisposable
{
    protected readonly HttpClient Client;
    protected readonly JsonSerializerOptions JsonSerializerOptions;
    protected readonly IServiceProvider ServiceProvider;

    private readonly WebApiFactory appFactory;

    protected IntegrationTestBase()
    {
        appFactory = new WebApiFactory(ConfigureServices, ConfigureAppConfiguration);

        Client = appFactory.CreateClient();
        JsonSerializerOptions = new JsonSerializerOptions();
        JsonSerializerOptions.TypeInfoResolverChain.Insert(0, ApiJsonSerializerContext.Default);
        ServiceProvider = appFactory.Services;
    }

    protected const string Username = "user";
    protected const string Password = "password";

    protected async Task RegisterAppUserAsync(UserRole role = UserRole.Owner)
    {
        var authService = appFactory.Services.GetRequiredService<IAuthService>();
        await authService.RegisterAsync(Username, Password, role);
    }

    protected async Task<AppUser?> GetAppUserAsync()
    {
        var repository = appFactory.Services.GetRequiredService<IUserRepository>();
        var user = await repository.FindAsync(username: Username);
        return user;
    }

    protected void Logout()
    {
        Client.DefaultRequestHeaders.Authorization = null;
    }

    protected virtual void ConfigureServices(IServiceCollection services) { }

    protected virtual void ConfigureAppConfiguration(IConfigurationBuilder builder) { }

    protected async Task<LoginResponse> LoginAsync(string? username = null, string? password = null)
    {
        var response = await Client.PostAsJsonAsync(ApiRoutes.Auth.PostLogin, new LoginRequest()
        {
            Username = username ?? Username,
            Password = password ?? Password
        });

        if (!response.IsSuccessStatusCode) throw new Exception("Could not login User.");

        var loginResponse = await response.Content.ReadFromJsonAsync<LoginResponse>();

        if (loginResponse?.Token is null) throw new Exception("Could not login User.");

        Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", loginResponse.Token);

        return loginResponse;
    }

    public void Dispose()
    {
        Client.Dispose();
        appFactory.Dispose();
    }
}
