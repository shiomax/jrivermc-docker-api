﻿using JRMC.Docker.DataAccess.Contracts;

namespace JRMC.Docker.WebApi.IntegrationTests.Tools;

internal class DatabaseSettings : IDatabaseSettings
{
    public string ConnectionString { get; internal set; } = string.Empty;
}
