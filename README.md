# JRiverMC Docker API

API for [jrivermc-docker](https://gitlab.com/shiomax/jrivermc-docker).

# Various Commands

- Build for production
```
dotnet publish -c Release -p:DebugType=None -p:DebugSymbols=false ./src/JRMC.Docker.WebApi/JRMC.Docker.WebApi.csproj
```
