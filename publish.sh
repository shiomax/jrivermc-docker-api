#!/bin/bash

# Validate working directory
[ -f ./jrivermc-docker-api.sln ] || { echo "Script should be run from project root."; exit 1; }

# Clear publish folder
rm -rf dist/*

# Determine architecture
PLATFORM=Unknown
if [ "$TARGETPLATFORM" = "linux/amd64" ]; then
    PLATFORM=linux-x64
elif [ "$TARGETPLATFORM" = "linux/arm64" ]; then
    PLATFORM=linux-arm64
elif [ "$TARGETPLATFORM" = "linux/arm/v7" ]; then
    PLATFORM=linux-arm
else
    echo "Unsupported platform '$TARGETPLATFORM'"
    exit 1
fi;

# Publish
dotnet publish \
    -c Release  \
    -r $PLATFORM \
    -p:DebugType=None \
    -p:DebugSymbols=false \
    --output ./dist \
    ./src/JRMC.Docker.WebApi/JRMC.Docker.WebApi.csproj

# Remove unused files
rm ./dist/appsettings.Development.json
